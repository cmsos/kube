#!/bin/bash

helm uninstall cmsos-kube-helm-gevb
kubectl get namespaces
kubectl wait --for=delete namespace/gevb --timeout=600s
