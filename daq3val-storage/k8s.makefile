#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2022, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
PackageName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF]}')
PackageType=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-1]}')
Package=$(PackageType)/$(PackageName)
ZONE_NAME=$(PackageName)

Summary=Kubernetes storage configuration

Description=This Helm chart provides persistent volumes for Elasticsearch cluster in k8s

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/slim
CHARTNAME=$(PROJECT_NAMESPACE)$(Project)-helm-$(PackageName)

#	helm template --set zone.name=$(PackageName) chart/$(CHARTNAME) > helm.debug

_all: all

default: all

all: clean
	echo "preparing storage..."

_package: package

package:
	mkdir -p chart/$(CHARTNAME)
	helm create chart/$(CHARTNAME)
	rm chart/$(CHARTNAME)/templates/*.yaml
	rm chart/$(CHARTNAME)/templates/tests/*.yaml
	cat _helpers.tpl.extension >> chart/$(CHARTNAME)/templates/_helpers.tpl
	cat values.yaml.extension >> chart/$(CHARTNAME)/values.yaml
	cp *.yaml chart/$(CHARTNAME)/templates/
	helm template chart/$(CHARTNAME) > helm.debug
	helm package --version "$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)" --destination chart chart/$(CHARTNAME)

_cleanall: clean

clean:
	echo "tidy up..."

_cleanpackage: cleanpackage

cleanpackage:
	rm -rf chart
	rm -f helm.debug

