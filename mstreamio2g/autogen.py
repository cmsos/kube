#!/usr/bin/python3

from string import Template
import getopt, sys

#Args used to parameterize
service=""
instance=0
numreplicas=1
interface='ens4f0'
ianame='mlx4_0'
gidtype='v2'
port=0
endpointhost='endpoint'
endpointport=0
endpointdomain='roce'
linktype='infiniband' # two options: "infiniband" or "roce"

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:i:a:t:p:e:m:f:l:n:s:"
long_options = ["help", "replicas=", "interface=", "ianame=", "gidtype=", "port=", "endpointhost=", "endpointport=", "endpointdomain=", "linktype=", "instance=", "service="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: autogen.py [-r #,--replicas=#] [-i name,--interface=name] [-a name,--ianame=name] [-t type,--gidtype=type] [-p # ,--port=#] [-e name,--endpointhost=name] [-m name,--endpointdomain=name] [-f # ,--endpointport=#] [-l name,--linktype=name] [-n # ,--instance=#] [-s name,--service=name]")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-i", "--interface"):
        interface = current_value
    elif current_argument in ("-a", "--ianame"):
        ianame = current_value
    elif current_argument in ("-t", "--gidtype"):
        gidtype = current_value
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-e", "--endpointhost"):
        server = current_value
    elif current_argument in ("-m", "--endpointdomain"):
        domain = current_value
    elif current_argument in ("-f", "--endpointport"):
        endpointport = int(current_value)
    elif current_argument in ("-l", "--linktype"):
        linktype = current_value
    elif current_argument in ("-n", "--instance"):
        instance = int(current_value)
    elif current_argument in ("-s", "--service"):
        service = current_value

#max block size * number of resources * number of replicas
rusenderpoolsize = 0x40000 * 120 * numreplicas
#max block size * (max block size * 2 + receive queue pair + margin) * number of replicas
bureceiverpoolsize = 0x40000 * (120 * 2 + 384 + 2) * numreplicas

#Set dictionary for template expansion
dict = {'endpointhost':endpointhost, 'endpointport':endpointport, 'endpointdomain':endpointdomain,
        'interface':interface, 'ianame':ianame, 'gidtype':gidtype, 'port':port, 'replica':'0',
        'rusenderpoolsize':rusenderpoolsize, 'bureceiverpoolsize':bureceiverpoolsize}
#Templates XML 

i2otarget=Template("""  <i2o:target class="$classname" instance="$replica" tid="$tid"/>""")

clientinfinibandt = Template("""
	<xc:Context url="http://client-$replica.client:$port">

		<xc:Endpoint protocol="ibv" service="i2o" hostname="client-$replica.client" port="$endpointport" network="datanet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000" connectOnRequest="true"/>
		
		<xc:Application class="pt::ibv::Application" id="11" network="local">
			<properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
				<iaName xsi:type="xsd:string">$ianame</iaName>
				<receiverPoolSize xsi:type="xsd:unsignedLong">0x100000</receiverPoolSize>
				<senderPoolSize xsi:type="xsd:unsignedLong">0xFA000000</senderPoolSize>
				<completionQueueSize xsi:type="xsd:unsignedInt">8192</completionQueueSize>
				<sendQueuePairSize xsi:type="xsd:unsignedInt">8192</sendQueuePairSize>
				<recvQueuePairSize xsi:type="xsd:unsignedInt">0</recvQueuePairSize>
				<maxMessageSize xsi:type="xsd:unsignedInt">257000</maxMessageSize>
				<deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
			</properties>
		</xc:Application>

		<xc:Application class="Client" id="10" instance="$replica" network="datanet">
			<properties xmlns="urn:xdaq-application:Client" xsi:type="soapenc:Struct">
				<maxFrameSize xsi:type="xsd:unsignedLong">257000</maxFrameSize>
				<committedPoolSize xsi:type="xsd:unsignedLong">0</committedPoolSize>
				<numberOfSamples xsi:type="xsd:unsignedLong">8</numberOfSamples>
				<sampleTime xsi:type="xsd:string">PT5S</sampleTime>
				<StdDev xsi:type="xsd:unsignedLong">0</StdDev>
				<currentSize xsi:type="xsd:unsignedLong">128</currentSize>
				<MinFragmentSize xsi:type="xsd:unsignedLong">32</MinFragmentSize>
				<MaxFragmentSize xsi:type="xsd:unsignedLong">32768</MaxFragmentSize>
				<randomDestination xsi:type="xsd:boolean">false</randomDestination>
				<fixedSize xsi:type="xsd:boolean">true</fixedSize>
				<createPool xsi:type="xsd:boolean">false</createPool>
				<poolName xsi:type="xsd:string">sibv</poolName>
			</properties>
		</xc:Application>

		<xc:Application class="xmem::probe::Application" id="21" network="local"/>

		<xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libmstreamio2g.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

	</xc:Context>""")

clientrocet = Template("""
	<xc:Context url="http://client-$replica.client:$port">

		<xc:Endpoint protocol="ibv" service="i2o" hostname="client-$replica.client" port="$endpointport" network="datanet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000" connectOnRequest="true"/>
		
		<xc:Application class="pt::ibv::Application" id="11" network="local">
			<properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
				<networkInterface xsi:type="xsd:string">$interface</networkInterface>
				<GIDType xsi:type="xsd:string">$gidtype</GIDType>
				<portNumber xsi:type="xsd:unsignedInt">1</portNumber>
				<receiverPoolSize xsi:type="xsd:unsignedLong">0x100000</receiverPoolSize>
				<senderPoolSize xsi:type="xsd:unsignedLong">0x2000000</senderPoolSize>
				<completionQueueSize xsi:type="xsd:unsignedInt">8192</completionQueueSize>
				<sendQueuePairSize xsi:type="xsd:unsignedInt">8192</sendQueuePairSize>
				<recvQueuePairSize xsi:type="xsd:unsignedInt">0</recvQueuePairSize>
				<maxMessageSize xsi:type="xsd:unsignedInt">257000</maxMessageSize>
				<deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
			</properties>
		</xc:Application>

		<xc:Application class="Client" id="10" instance="$replica" network="datanet">
			<properties xmlns="urn:xdaq-application:Client" xsi:type="soapenc:Struct">
				<maxFrameSize xsi:type="xsd:unsignedLong">257000</maxFrameSize>
				<committedPoolSize xsi:type="xsd:unsignedLong">1870000000</committedPoolSize>
				<numberOfSamples xsi:type="xsd:unsignedLong">8</numberOfSamples>
				<sampleTime xsi:type="xsd:string">PT5S</sampleTime>
				<StdDev xsi:type="xsd:unsignedLong">0</StdDev>
				<currentSize xsi:type="xsd:unsignedLong">128</currentSize>
				<MinFragmentSize xsi:type="xsd:unsignedLong">32</MinFragmentSize>
				<MaxFragmentSize xsi:type="xsd:unsignedLong">32768</MaxFragmentSize>
				<randomDestination xsi:type="xsd:boolean">false</randomDestination>
				<fixedSize xsi:type="xsd:boolean">true</fixedSize>
				<createPool xsi:type="xsd:boolean">false</createPool>
				<poolName xsi:type="xsd:string">sibv</poolName>
			</properties>
		</xc:Application>

		<xc:Application class="xmem::probe::Application" id="21" network="local"/>

		<xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libmstreamio2g.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

	</xc:Context>""")

serverinfinibandt = Template("""
	<xc:Context url="http://server-$replica.server:$port">

		<xc:Endpoint protocol="ibv" service="i2o" hostname="server-$replica.server" port="$endpointport" network="datanet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000" connectOnRequest="true"/>

		<xc:Application class="pt::ibv::Application" id="12" network="local">
			<properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
				<iaName xsi:type="xsd:string">$ianame</iaName>
				<receiverPoolSize xsi:type="xsd:unsignedLong">0xFA000000</receiverPoolSize>
				<senderPoolSize xsi:type="xsd:unsignedLong">0x100000</senderPoolSize>
				<completionQueueSize xsi:type="xsd:unsignedInt">8192</completionQueueSize>
				<sendQueuePairSize xsi:type="xsd:unsignedInt">0</sendQueuePairSize>
				<recvQueuePairSize xsi:type="xsd:unsignedInt">64</recvQueuePairSize>
				<maxMessageSize xsi:type="xsd:unsignedInt">257000</maxMessageSize>
				<deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
			</properties>
		</xc:Application>

		<xc:Application class="Server" id="15" instance="$replica" network="datanet">
			<properties xmlns="urn:xdaq=application:Server" xsi:type="soapenc:Struct">
				<currentSize xsi:type="xsd:unsignedLong">0</currentSize>
				<numberOfSamples xsi:type="xsd:unsignedLong">8</numberOfSamples>
				<sampleTime xsi:type="xsd:string">PT5S</sampleTime>
			</properties>
		</xc:Application>

		<xc:Application class="xmem::probe::Application" id="21" network="local"/>

		<xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libmstreamio2g.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

	</xc:Context>""")

serverrocet = Template("""
	<xc:Context url="http://server-$replica.server:$port">

		<xc:Endpoint protocol="ibv" service="i2o" hostname="server-$replica.server" port="$endpointport" network="datanet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000" connectOnRequest="true"/>

		<xc:Application class="pt::ibv::Application" id="12" network="local">
			<properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
				<networkInterface xsi:type="xsd:string">$interface</networkInterface>
				<GIDType xsi:type="xsd:string">$gidtype</GIDType>
				<portNumber xsi:type="xsd:unsignedInt">1</portNumber>
				<receiverPoolSize xsi:type="xsd:unsignedLong">0x40000000</receiverPoolSize>
				<senderPoolSize xsi:type="xsd:unsignedLong">0x100000</senderPoolSize>
				<completionQueueSize xsi:type="xsd:unsignedInt">8192</completionQueueSize>
				<sendQueuePairSize xsi:type="xsd:unsignedInt">0</sendQueuePairSize>
				<recvQueuePairSize xsi:type="xsd:unsignedInt">1024</recvQueuePairSize>
				<maxMessageSize xsi:type="xsd:unsignedInt">257000</maxMessageSize>
				<deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
			</properties>
		</xc:Application>

		<xc:Application class="Server" id="15" instance="$replica" network="datanet">
			<properties xmlns="urn:xdaq=application:Server" xsi:type="soapenc:Struct">
				<currentSize xsi:type="xsd:unsignedLong">0</currentSize>
				<numberOfSamples xsi:type="xsd:unsignedLong">8</numberOfSamples>
				<sampleTime xsi:type="xsd:string">PT5S</sampleTime>
			</properties>
		</xc:Application>

		<xc:Application class="xmem::probe::Application" id="21" network="local"/>

		<xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libmstreamio2g.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
		<xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

	</xc:Context>""")

#Generate configuration XML 


print("""<xc:Partition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30">""")
print("""<i2o:protocol xmlns:i2o="http://xdaq.web.cern.ch/xdaq/xsd/2004/I2OConfiguration-30">""")
t = 24
for x in range(numreplicas):
    if (service == 'client'):
        if (x == instance):
            i = i2otarget.substitute(replica=x, tid=t, classname='Client')
            print (i)
        t += 1
        i = i2otarget.substitute(replica=x, tid=t, classname='Server')
        print (i)
        t += 1
    elif (service == 'server'):
        t += 1
        if (x == instance):
            i = i2otarget.substitute(replica=x, tid=t, classname='Server')
            print (i)
        t += 1

print("""</i2o:protocol>""")
for x in range(numreplicas):
    dict['replica'] = x
    if linktype == 'infiniband':
        if (service == 'client'):
            if (x == instance):
                print(clientinfinibandt.substitute(dict))
            print(serverinfinibandt.substitute(dict))
        elif (service == 'server'):
            if (x == instance):
                print(serverinfinibandt.substitute(dict))
    else:
        if (service == 'client'):
            if (x == instance):
                print(clientrocet.substitute(dict))
            print(serverrocet.substitute(dict))
        elif (service == 'server'):
            if (x == instance):
                print(serverrocet.substitute(dict))
print ("""</xc:Partition>""")
