# /users/smorovic/10streams_fixed_simple/V1 (CMSSW_8_0_0)

import FWCore.ParameterSet.Config as cms

process = cms.Process( "HLT" )

process.HLTConfigVersion = cms.PSet(
  tableName = cms.string('/users/smorovic/10streams_fixed_simple/V1')
)

process.transferSystem = cms.PSet( 
  destinations = cms.vstring( 'Tier0',
    'DQM',
    'ECAL',
    'EventDisplay',
    'Lustre',
    'None' ),
  transferModes = cms.vstring( 'default',
    'test',
    'emulator' ),
  streamA = cms.PSet( 
    default = cms.vstring( 'Tier0' ),
    test = cms.vstring( 'Lustre' ),
    emulator = cms.vstring( 'Lustre' )
  ),
  streamDQM = cms.PSet( 
    default = cms.vstring( 'DQM' ),
    test = cms.vstring( 'DQM',
      'Lustre' ),
    emulator = cms.vstring( 'None' )
  ),
  default = cms.PSet( 
    default = cms.vstring( 'Tier0' ),
    test = cms.vstring( 'Lustre' ),
    emulator = cms.vstring( 'Lustre' )
  ),
  streamDQMHistograms = cms.PSet( 
    default = cms.vstring( 'DQM' ),
    test = cms.vstring( 'DQM',
      'Lustre' ),
    emulator = cms.vstring( 'None' )
  ),
  streamHLTRates = cms.PSet( 
    default = cms.vstring( 'Lustre' ),
    test = cms.vstring( 'Lustre' ),
    emulator = cms.vstring( 'None' )
  )
)
process.streams = cms.PSet( 
  A = cms.vstring( 'A1' ),
  B1 = cms.vstring( 'B1D' ),
  B2 = cms.vstring( 'B2D' ),
  B3 = cms.vstring( 'B3D' ),
  B4 = cms.vstring( 'B4D' ),
  B5 = cms.vstring( 'B5D' ),
  B6 = cms.vstring( 'B6D' ),
  B7 = cms.vstring( 'B7D' ),
  B8 = cms.vstring( 'B8D' ),
  DQM = cms.vstring( 'DQM1' )
)
process.datasets = cms.PSet( 
  A1 = cms.vstring( 'pA' ),
  B1D = cms.vstring( 'pB1' ),
  B2D = cms.vstring( 'pB2' ),
  B3D = cms.vstring( 'pB3' ),
  B4D = cms.vstring( 'pB4' ),
  B5D = cms.vstring( 'pB5' ),
  B6D = cms.vstring( 'pB6' ),
  B7D = cms.vstring( 'pB7' ),
  B8D = cms.vstring( 'pB8' ),
  DQM1 = cms.vstring( 'pDQM' )
)

process.source = cms.Source( "FedRawDataInputSource",
		getLSFromFilename = cms.untracked.bool( True ),
		verifyChecksum = cms.untracked.bool( True ),
		useL1EventID = cms.untracked.bool( True ),
		eventChunkSize = cms.untracked.uint32( 32 ),
		eventChunkBlock = cms.untracked.uint32( 32 ),
		numBuffers = cms.untracked.uint32(2),
		maxBufferedFiles = cms.untracked.uint32(2)
)

process.PoolDBESSource = cms.ESSource( "PoolDBESSource",
    globaltag = cms.string( "80X_dataRun2_HLT_v4" ),
    RefreshEachRun = cms.untracked.bool( False ),
    snapshotTime = cms.string( "" ),
    toGet = cms.VPSet( 
    ),
    DBParameters = cms.PSet( 
      authenticationPath = cms.untracked.string( "." ),
      connectionRetrialTimeOut = cms.untracked.int32( 60 ),
      idleConnectionCleanupPeriod = cms.untracked.int32( 10 ),
      messageLevel = cms.untracked.int32( 0 ),
      enablePoolAutomaticCleanUp = cms.untracked.bool( False ),
      enableConnectionSharing = cms.untracked.bool( True ),
      enableReadOnlySessionOnUpdateConnection = cms.untracked.bool( False ),
      connectionTimeOut = cms.untracked.int32( 0 ),
      authenticationSystem = cms.untracked.int32( 0 ),
      connectionRetrialPeriod = cms.untracked.int32( 10 )
    ),
    RefreshAlways = cms.untracked.bool( False ),
    connect = cms.string( "frontier://(proxyurl=http://localhost:3128)(serverurl=http://localhost:8000/FrontierOnProd)(serverurl=http://localhost:8000/FrontierOnProd)(retrieve-ziplevel=0)/CMS_CONDITIONS" ),
    ReconnectEachRun = cms.untracked.bool( False ),
    RefreshOpenIOVs = cms.untracked.bool( False ),
    DumpStat = cms.untracked.bool( False )
)

process.DQMStore = cms.Service( "DQMStore",
    referenceFileName = cms.untracked.string( "" ),
    verbose = cms.untracked.int32( 0 ),
    collateHistograms = cms.untracked.bool( False ),
    enableMultiThread = cms.untracked.bool( True ),
    forceResetOnBeginLumi = cms.untracked.bool( False ),
    saveByLumi = cms.untracked.bool( False ),
    LSbasedMode = cms.untracked.bool( True ),
    verboseQT = cms.untracked.int32( 0 )
)
process.EvFDaqDirector = cms.Service( "EvFDaqDirector",
		useFileBroker = cms.untracked.bool(True),
		fileBrokerHostFromCfg = cms.untracked.bool(True),
		fileBrokerHost = cms.untracked.string(""),
		runNumber = cms.untracked.uint32( 0 ),
		baseDir = cms.untracked.string( "." ),
		buBaseDir = cms.untracked.string( "." ),
		selectedTransferMode = cms.untracked.string("")
)
process.FastMonitoringService = cms.Service( "FastMonitoringService",
    sleepTime = cms.untracked.int32( 1 )
)
process.PrescaleService = cms.Service( "PrescaleService",
    forceDefault = cms.bool( False ),
    prescaleTable = cms.VPSet( 
      cms.PSet(  pathName = cms.string( "pDQM" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB8" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB7" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB6" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB5" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB4" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB3" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB2" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pB1" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      ),
      cms.PSet(  pathName = cms.string( "pA" ),
        prescales = cms.vuint32( 1000, 100, 1000, 500, 1000, 1000, 1000, 1000, 1000 )
      )
    ),
    #lvl1DefaultLabel = cms.string( "1e33" ),
    lvl1DefaultLabel = cms.string( "1e32" ),
    lvl1Labels = cms.vstring( '2e33',
      '1.4e33',
      '1e33',
      '7e32',
      '5e32',
      '3e32',
      '2e32',
      '1.4e32',
      '1e32' )
)
process.MessageLogger = cms.Service( "MessageLogger",
    suppressInfo = cms.untracked.vstring( 'hltGtDigis' ),
    debugs = cms.untracked.PSet( 
      threshold = cms.untracked.string( "INFO" ),
      placeholder = cms.untracked.bool( True ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    suppressDebug = cms.untracked.vstring(  ),
    cout = cms.untracked.PSet( 
      threshold = cms.untracked.string( "ERROR" ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    cerr_stats = cms.untracked.PSet( 
      threshold = cms.untracked.string( "WARNING" ),
      output = cms.untracked.string( "cerr" ),
      optionalPSet = cms.untracked.bool( True )
    ),
    warnings = cms.untracked.PSet( 
      threshold = cms.untracked.string( "INFO" ),
      placeholder = cms.untracked.bool( True ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    statistics = cms.untracked.vstring( 'cerr' ),
    cerr = cms.untracked.PSet( 
      INFO = cms.untracked.PSet(  limit = cms.untracked.int32( 0 ) ),
      noTimeStamps = cms.untracked.bool( False ),
      FwkReport = cms.untracked.PSet( 
        reportEvery = cms.untracked.int32( 1 ),
        limit = cms.untracked.int32( 0 )
      ),
      default = cms.untracked.PSet(  limit = cms.untracked.int32( 10000000 ) ),
      Root_NoDictionary = cms.untracked.PSet(  limit = cms.untracked.int32( 0 ) ),
      FwkJob = cms.untracked.PSet(  limit = cms.untracked.int32( 0 ) ),
      FwkSummary = cms.untracked.PSet( 
        reportEvery = cms.untracked.int32( 1 ),
        limit = cms.untracked.int32( 10000000 )
      ),
      threshold = cms.untracked.string( "INFO" ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    FrameworkJobReport = cms.untracked.PSet( 
      default = cms.untracked.PSet(  limit = cms.untracked.int32( 0 ) ),
      FwkJob = cms.untracked.PSet(  limit = cms.untracked.int32( 10000000 ) )
    ),
    suppressWarning = cms.untracked.vstring( 'hltGtDigis' ),
    errors = cms.untracked.PSet( 
      threshold = cms.untracked.string( "INFO" ),
      placeholder = cms.untracked.bool( True ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    fwkJobReports = cms.untracked.vstring( 'FrameworkJobReport' ),
    debugModules = cms.untracked.vstring(  ),
    infos = cms.untracked.PSet( 
      threshold = cms.untracked.string( "INFO" ),
      Root_NoDictionary = cms.untracked.PSet(  limit = cms.untracked.int32( 0 ) ),
      placeholder = cms.untracked.bool( True ),
      suppressInfo = cms.untracked.vstring(  ),
      suppressWarning = cms.untracked.vstring(  ),
      suppressDebug = cms.untracked.vstring(  ),
      suppressError = cms.untracked.vstring(  )
    ),
    categories = cms.untracked.vstring( 'FwkJob',
      'FwkReport',
      'FwkSummary',
      'Root_NoDictionary' ),
    destinations = cms.untracked.vstring( 'warnings',
      'errors',
      'infos',
      'debugs',
      'cout',
      'cerr' ),
    threshold = cms.untracked.string( "INFO" ),
    suppressError = cms.untracked.vstring( 'hltGtDigis' )
)

process.DQMFileSaver = cms.EDAnalyzer( "DQMFileSaver",
    runIsComplete = cms.untracked.bool( False ),
    referenceHandling = cms.untracked.string( "all" ),
    producer = cms.untracked.string( "DQM" ),
    forceRunNumber = cms.untracked.int32( -1 ),
    saveByRun = cms.untracked.int32( 1 ),
    saveAtJobEnd = cms.untracked.bool( False ),
    saveByLumiSection = cms.untracked.int32( 1 ),
    version = cms.untracked.int32( 1 ),
    referenceRequireStatus = cms.untracked.int32( 100 ),
    convention = cms.untracked.string( "FilterUnit" ),
    dirName = cms.untracked.string( "." ),
    fileFormat = cms.untracked.string( "PB" )
)

process.ExceptionGenerator2 = cms.EDAnalyzer( "ExceptionGenerator",
    defaultAction = cms.untracked.int32( 0 ),
    defaultQualifier = cms.untracked.int32( 0 )
)
process.hltPrepDQM = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB8 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB7 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB6 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB5 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB4 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB3 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB2 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.hltPrepB1 = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)
process.ExceptionGenerator = cms.EDAnalyzer( "ExceptionGenerator",
    defaultAction = cms.untracked.int32( 0 ),
    defaultQualifier = cms.untracked.int32( 0 )
)
process.hltPrepA = cms.EDFilter( "HLTPrescaler",
    L1GtReadoutRecordTag = cms.InputTag( "hltGtDigis" ),
    offset = cms.uint32( 0 )
)

process.hltOutputB8 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB8' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB7 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB7' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB6 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB6' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB5 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB5' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB4 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB4' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB3 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB3' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB2 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB2' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputB1 = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pB1' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputDQM = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pDQM' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)
process.hltOutputA = cms.OutputModule( "ShmStreamConsumer",
    SelectEvents = cms.untracked.PSet(  SelectEvents = cms.vstring( 'pA' ) ),
    outputCommands = cms.untracked.vstring( 'drop *',
      'keep FEDRawDataCollection_rawDataCollector_*_*',
      'keep FEDRawDataCollection_source_*_*' )
)

process.hltHLTriggerJSONMonitoring = cms.EDAnalyzer( "HLTriggerJSONMonitoring",
    triggerResults = cms.InputTag( 'TriggerResults','','HLT' )
)


process.epB8 = cms.EndPath( process.hltOutputB8 )
process.epB7 = cms.EndPath( process.hltOutputB7 )
process.epB6 = cms.EndPath( process.hltOutputB6 )
process.epB5 = cms.EndPath( process.hltOutputB5 )
process.epB4 = cms.EndPath( process.hltOutputB4 )
process.epB3 = cms.EndPath( process.hltOutputB3 )
process.epB2 = cms.EndPath( process.hltOutputB2 )
process.epB1 = cms.EndPath( process.hltOutputB1 )
process.pDQMhisto = cms.Path( process.DQMFileSaver )
process.epDQM = cms.EndPath( process.hltOutputDQM )
process.ep1 = cms.EndPath( process.hltOutputA )
process.pDQM = cms.Path( process.ExceptionGenerator2 + process.hltPrepDQM )
process.pB8 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB8 )
process.pB7 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB7 )
process.pB6 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB6 )
process.pB5 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB5 )
process.pB4 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB4 )
process.pB3 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB3 )
process.pB2 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB2 )
process.pB1 = cms.Path( process.ExceptionGenerator2 + process.hltPrepB1 )
process.pA = cms.Path( process.ExceptionGenerator + process.hltPrepA )

process.RatesMonitoring = cms.EndPath(process.hltHLTriggerJSONMonitoring)

import FWCore.ParameterSet.VarParsing as VarParsing 

import os 

cmsswbase = os.path.expandvars('$CMSSW_BASE/') 

options = VarParsing.VarParsing ('analysis') 

options.register ('runNumber', 
                  1, # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.int,          # string, int, or float 
                  "Run Number") 

options.register ('buBaseDir', 
                  '/fff/BU0', # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.string,       # string, int, or float 
                  "BU base directory") 

options.register ('dataDir', 
                  '/fff/data', # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.string,       # string, int, or float 
                  "FU data directory") 

options.register ('numThreads', 
                  4, # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.int,          # string, int, or float 
                  "Number of CMSSW threads") 

options.register ('numFwkStreams', 
                  4, # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.int,          # string, int, or float 
                  "Number of CMSSW streams") 

options.register ('fileBrokerHost',
                  '', # default value
                  VarParsing.VarParsing.multiplicity.singleton,
                  VarParsing.VarParsing.varType.string,       # string, int, or float
                  "File broker host data network address")

options.register ('transferMode', 
                  '', # default value 
                  VarParsing.VarParsing.multiplicity.singleton, 
                  VarParsing.VarParsing.varType.string,       # string, int, or float 
                  "Selected transfer mode propagated by RCMS") 



options.parseArguments() 

process.options = cms.untracked.PSet( 
    numberOfThreads = cms.untracked.uint32(options.numThreads), 
    numberOfStreams = cms.untracked.uint32(options.numFwkStreams) 
) 

process.EvFDaqDirector.buBaseDir    = options.buBaseDir 
process.EvFDaqDirector.baseDir      = options.dataDir 
process.EvFDaqDirector.runNumber    = options.runNumber 

try: 
     process.EvFDaqDirector.selectedTransferMode = options.transferMode 
except: 
     print "unable to set process.EvFDaqDirector.selectedTransferMode=",         options.transferMode 
for moduleName in process.__dict__['_Process__outputmodules']:
    modified_module = getattr(process,moduleName)
    modified_module.compression_level=cms.untracked.int32(1)
    modified_module.compression_algorithm=cms.untracked.string("LZMA")

try:
    process.hltEnableParking.result = True
except:
    pass

try:
    process.EvFDaqDirector.useFileBroker  = True
except:
    print "no process.EvFDaqDirector.useFileBroker in Python configuration"

if options.fileBrokerHost:
    try:
        process.EvFDaqDirector.fileBrokerHostFromCfg = False
    except:
        print "Unable to set process.EvFDaqDirector.fileBrokerHostFromCfg = True"
    try:
        process.EvFDaqDirector.fileBrokerHost = options.fileBrokerHost
    except:
        print "Unable to set process.EvFDaqDirector.fileBrokerHost =",options.fileBrokerHost

