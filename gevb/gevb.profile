<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
  <!-- Health -->
  <xp:Application class="health::Application" id="300" group="profile" service="health" network="local"/>
  <xp:Module>${XDAQ_ROOT}/lib/libhealth.so</xp:Module>

  <!-- Compulsory Plugins -->
  <xp:Application class="executive::Application" id="0" group="profile" service="executive" network="local">
    <properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
      <logUrl xsi:type="xsd:string">console</logUrl>
      <logLevel xsi:type="xsd:string">INFO</logLevel>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

  <xp:Application class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" service="pthttp">
    <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
      <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
      <aliasName xsi:type="xsd:string">tmp</aliasName>
      <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
      <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[6]">
        <item xsi:type="soapenc:Struct" soapenc:position="[0]">
          <name xsi:type="xsd:string">image/png</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[1]">
          <name xsi:type="xsd:string">image/jpg</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[2]">
          <name xsi:type="xsd:string">image/gif</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[3]">
          <name xsi:type="xsd:string">application/x-shockwave-flash</name>
          <value xsi:type="xsd:string">PT120H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[4]">
          <name xsi:type="xsd:string">application/font-woff</name>
          <value xsi:type="xsd:string">PT8600H</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[5]">
          <name xsi:type="xsd:string">text/css</name>
          <value xsi:type="xsd:string">PT4300H</value>
        </item>
      </expiresByType>
      <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
        <item xsi:type="soapenc:Struct" soapenc:position="[0]">
          <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
          <value xsi:type="xsd:string">*</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[1]">
          <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
          <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
        </item>
        <item xsi:type="soapenc:Struct" soapenc:position="[2]">
          <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
          <value xsi:type="xsd:string">x-requested-with, soapaction</value>
        </item>
      </httpHeaderFields>
    </properties>
  </xp:Application>
  <xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

  <xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" service="ptfifo"/>
  <xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>

  <!-- XRelay -->
  <xp:Application class="xrelay::Application" id="4"  service="xrelay" group="profile" network="local"/>
  <xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module>

  <!-- HyperDAQ -->
  <xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
  <xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>

  <pol:policy xmlns:pol="http://xdaq.web.cern.ch/xdaq/xsd/2013/XDAQPolicy-10">
    <pol:element pattern="urn:toolbox-task-workloop:pt::ibv::acceptor-(.+)/waiting" type="thread" package="numa" mempolicy="onnode" memnode="0" cpunodes="0"/>
    <pol:element pattern="urn:toolbox-task-workloop:http-(.+)/waiting" package="numa" type="thread" mempolicy="onnode" memnode="0" cpunodes="0"/>
    <pol:element pattern="urn:toolbox-task-workloop:fifo/PeerTransport/waiting" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="4"/>
    <pol:element pattern="urn:toolbox-task-workloop:pt::ibv::completionworkloops/polling" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="6"/>
    <pol:element pattern="urn:toolbox-task-workloop:pt::ibv::completionworkloopr-(.+)/polling" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="8"/>
    <pol:element pattern="urn:toolbox-mem-allocator:CommittedHeapAllocator" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="gevb2g-ru-loop-(.+)" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="10" />
    <pol:element pattern="urn:toolbox-task-workloop:gevb2g-InputEmulator-0/polling" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="12"/>
    <pol:element pattern="urn:toolbox-mem-allocator-ibv-receiver-(.+)" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:toolbox-mem-allocator-ibv-sender-(.+)" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:gevb2g-RU-rlist-inputdataFIFO:alloc" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:gevb2g-RU-rlist-requestqueue:alloc" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:undefined:alloc" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:eventCompleted:alloc" type="alloc" package="numa" mempolicy="onnode" node="0"/>
    <pol:element pattern="urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling" type="thread" package="numa" mempolicy="onnode" memnode="0" affinity="14"/>
  </pol:policy>

</xp:Profile>
