#!/bin/bash

helm uninstall cmsos-kube-helm-shubml
kubectl get namespaces
kubectl wait --for=delete namespace/shubml --timeout=600s
