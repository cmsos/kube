import soap


def retrieve(port,replicas):
    sample = {}
    m  = {}
    fragmentRate = 0
    for r in range(replicas):
        m['BU'+ str(r)] = int(soap.parameter_get("http://node-"+str(r)+".node:"+str(port), "shub::NodeRC", r, "eventRate", "xsd:unsignedInt"))
        fragmentRate += int(m['BU'+ str(r)])
    for r in range(replicas):
        m['RU'+ str(r)] = fragmentRate
    m['EVM0'] = fragmentRate

    sample['rates'] = m;

    s  = {}
    s['EVM0'] = 0
    eventSize = 0
    for r in range(replicas):
        s['RU'+ str(r)] = int(soap.parameter_get("http://node-" + str(r)+ ".node:" + str(port), "shub::NodeRC", r, "superFragmentSize", "xsd:unsignedInt"))
        eventSize +=  int(s['RU'+ str(r)])
    for r in range(replicas):
        s['BU'+ str(r)] = eventSize
    sample['sizes'] = s;

    print ("retrieved sample " + str(sample))
    return sample
