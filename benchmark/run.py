#!/usr/bin/python3

import time
import getopt, sys
import http.client
from urllib.parse import urlparse

print("From run.py", flush=True)

def simple_get(url):
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port, timeout=1)
        conn.request("GET", "/")
        response = conn.getresponse()
        status = response.status
        conn.close()
        return status
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )
    except Fault as ex:
        raise ex

print("From run.py after function definition", flush=True)

#Args used to parameterize 
numreplicas=1
port=0
domainname=""

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:p:d:"
long_options = ["help", "replicas=", "port=", "domainname="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print(str(err), flush=True)
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: run.py [-r #,--replicas=#] [-p # ,--port=#] [-d <name> ,--domainname=<name>]")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-d", "--domainname"):
        domainname = current_value
    else:
        print("Invalid option", flush=True)
        sys.exit(2)

print("Wait for benchmark to be ready...", flush=True)
expected = numreplicas*2
connected = False

print("expected=" + str(expected), flush=True)
while not connected:
    total = 0
    try:
        for r in range(numreplicas):
            c = int(simple_get("http://client-" + str(r) + ".client" + domainname + ":" + str(port)))
            print("Return value from parameter get is " + str(c), flush=True)
            if (c == 200):
                total += 1
            c = simple_get("http://server-" + str(r) + ".server" + domainname + ":" + str(port))
            print("Return value from parameter get is " + str(c), flush=True)
            if (c == 200):
                total += 1
    except Exception as ex:
        print(ex)
    except Fault as ex:
        print(ex)

    if (total == expected):
        connected = True
    else:
        time.sleep(1)
    print("Found " + str(total) + " nodes out of " + str(expected), flush=True)
print("done liveness.", flush=True)

f = open("/tmp/runner-alive", "x")
f.close()
