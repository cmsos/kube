// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

//
// Version definition for play 
//
#ifndef _play_version_h_
#define _play_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PLAY_VERSION_MAJOR 1
#define PLAY_VERSION_MINOR 0
#define PLAY_VERSION_PATCH 0
// If any previous versions available E.g. #define SHUBPREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PLAY_PREVIOUS_VERSIONS


//
// Template macros
//
#define PLAY_VERSION_CODE PACKAGE_VERSION_CODE(PLAY_VERSION_MAJOR,GEVB2_GVERSION_MINOR,PLAY_VERSION_PATCH)
#ifndef PLAY_PREVIOUS_VERSIONS
#define PLAY_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PLAY_VERSION_MAJOR,PLAY_VERSION_MINOR,PLAY_VERSION_PATCH)
#else 
#define PLAY_FULL_VERSION_LIST  PLAY_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PLAY_VERSION_MAJOR,PLAY_VERSION_MINOR,PLAY_VERSION_PATCH)
#endif 

namespace play 
{
const std::string project = "sidecar";
const std::string package  =  "play";
const std::string versions = PLAY_FULL_VERSION_LIST;
const std::string summary = "Metrics gnerator";
const std::string description = "";
const std::string authors = "Luciano Orsini, Dainius Simelevicius";
const std::string link = "http://xdaq.cern.ch/";
config::PackageInfo getPackageInfo();
void checkPackageDependencies() ;
std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
