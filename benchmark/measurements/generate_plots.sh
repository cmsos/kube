#!/bin/bash

gnuplot -e "format='jpeg'" startup_plot_detailed.gp
gnuplot -e "format='eps'" startup_plot_detailed.gp
epstopdf startup_plot_detailed.eps

gnuplot -e "format='jpeg'" termination_plot.gp
gnuplot -e "format='eps'" termination_plot.gp
epstopdf termination_plot.eps

gnuplot -e "format='jpeg'" startup_plot_more_pods.gp
gnuplot -e "format='eps'" startup_plot_more_pods.gp
epstopdf startup_plot_more_pods.eps
