// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "version.h"
#include "xdaq/version.h"

GETPACKAGEINFO(play)

void play::checkPackageDependencies() 
{
	CHECKDEPENDENCY(xdaq)
}

std::set<std::string, std::less<std::string> > play::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,xdaq);
	return dependencies;
}	
