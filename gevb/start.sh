#!/bin/bash

helm install --set zone.name=gevb,zone.application.replicas=4 cmsos-kube-helm-gevb chart/cmsos-kube-helm-gevb-1.0.0.tgz

#Extracting pod name
podname=$(kubectl get pods -l application=runner -n gevb -o jsonpath='{.items[0].metadata.name}')

echo "podname = "$podname

status="False"
until [ "$status" = "True" ];
do
  status=$(kubectl get pod $podname -n gevb -o jsonpath='{.status.conditions[?(@.type=="Ready")].status}')
  if [ -z "$status" ]
  then
    status="False"
  fi
done;

kubectl get pods -n gevb
