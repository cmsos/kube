#!/bin/bash

type=
while getopts t:r: flag
do
  case "${flag}" in
    t) type=${OPTARG};;
    r) replicas=${OPTARG};;
  esac
done

echo "type="$type
echo "replicas="$replicas

if [[ $type == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

if [[ $replicas == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

rm output.txt
touch output.txt
for (( i=1; i<=$replicas; i++ ))
do
  echo "i="$i
  #Uninstalling mstreamio2g chart
  helm uninstall cmsos-kube-helm-mstreamio2g
  kubectl get namespaces
  kubectl wait --for=delete namespace/mstreamio2g --timeout=600s
  echo "mstreamio2g namespace deleted"
  kubectl get namespaces

  case "$type" in
    clean) wasshru 'sudo docker rmi gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
    preloaded) wasshru 'sudo docker pull gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
    skip) ;;
  esac

  start_time=$(date +%s)

  helm install --set zone.name=mstreamio2g,zone.application.replicas=$i cmsos-kube-helm-mstreamio2g chart/cmsos-kube-helm-mstreamio2g-3.0.0.tgz
  clientcount=$(kubectl get endpoints client -n mstreamio2g -o=jsonpath='{.subsets[0].addresses}' | jq length)
  if [ -z "$clientcount" ]
  then
    clientcount=0
  fi
  until [ "$clientcount" -eq "$i" ];
  do
    echo "clientcount = "$clientcount;
    clientcount=$(kubectl get endpoints client -n mstreamio2g -o=jsonpath='{.subsets[0].addresses}' | jq length)
    if [ -z "$clientcount" ]
    then
      clientcount=0
    fi
  done;
  echo "clientcount = "$clientcount;

  servercount=$(kubectl get endpoints server -n mstreamio2g -o=jsonpath='{.subsets[0].addresses}' | jq length)
  if [ -z "$servercount" ]
  then
    servercount=0
  fi
  until [ "$servercount" -eq "$i" ];
  do
    echo "servercount = "$servercount;
    servercount=$(kubectl get endpoints server -n mstreamio2g -o=jsonpath='{.subsets[0].addresses}' | jq length)
    if [ -z "$servercount" ]
    then
      servercount=0
    fi
  done;
  echo "servercount = "$servercount;

  end_time=$(date +%s)
  elapsed=$(( end_time - start_time ))

  echo "number of replicas = "$i" time elapsed="$elapsed"s"
  echo $i $elapsed >> output.txt
done
