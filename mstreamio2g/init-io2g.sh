#!/bin/sh

echo "initialize pt:utcp::Application "$1
curl --connect-timeout 5 \
     --retry 20 \
     --stderr /dev/null $1
