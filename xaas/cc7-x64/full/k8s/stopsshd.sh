#!/bin/bash

export KUBECONFIG=~/.kube/config
kubectl delete secret sshkey
kubectl delete service sshd-jumpserver-svc
kubectl delete deployment sshd-jumpserver-deployment
