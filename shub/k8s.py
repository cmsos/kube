import ssl
ssl._create_default_https_context = ssl._create_unverified_context

import http.client
from urllib.parse import urlparse
import sys, json

def api(host, port, path, token):
	print('host = ' + host)
	print('port = ' + str(port))
	print('path = ' + path)
	#print('token = ' + token)
	headers = {'Authorization': 'Bearer ' + token}
	conn = http.client.HTTPSConnection(host, port)
	conn.request("GET", path, "", headers)
	response = conn.getresponse()
	replymessage = str(response.read().decode())
	#print(replymessage)
	return replymessage
