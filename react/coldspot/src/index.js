import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { Route } from 'react-router';
import { BrowserRouter, withRouter } from 'react-router-dom';
import queryString from 'query-string';

import 'date-fns';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';

import { blue } from '@material-ui/core/colors';
import LensIcon from '@material-ui/icons/Lens';
import AcUnitIcon from '@material-ui/icons/AcUnit';

class JSONView extends React.Component {
  render() {
    return (
      <div>
        <pre>
          <code>
            {JSON.stringify(this.props.exception, null, 2)}
          </code>
        </pre>
      </div>
    );
  }
}

class HTMLView extends React.Component {
  generateHTML(input) {
    var originatedby = "";
    if ("children" in input) {
      originatedby = "originated by " + this.generateHTML(input.children[0]);
    }

    var output = "<b>" + input.label + "</b>\n";
    output += "<br>\n  <table>\n";

    for (const property of input.properties) {
      output += "    <tr>\n";
      output += "      <td>\n";
      output += "        " + property.name + "\n";
      output += "      </td>\n";
      var value = property.value;
      if (property.name === "message") {
        value = decodeURI(value);
      }
      output += "      <td>\n";
      output += "        " + value + "\n";
      output += "      </td>\n";
      output += "    </tr>\n";
    }

    output += "  </table>\n<br>\n";
    output += originatedby;
    return output;
  }

  render () {
    return (
      <div>
        <pre>
          <code>
            {this.generateHTML(this.props.exception)}
          </code>
        </pre>
      </div>
    );
  }
}

class HierarchicalView extends React.Component {
  styles

  constructor(props) {
    super(props);

    this.styles = makeStyles({
      root: {
        height: 216,
        flexGrow: 1,
        maxWidth: 400,
      },
      table: {
        minWidth: 650,
      },
    });

    this.state = {
      expanded: [],
      selected: []
    }   
  }

  generateTreeItems(exception) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }
    if ("children" in exception) {
      return (
        <TreeItem nodeId={uniqueid} label={exception.label}>{this.generateTreeItems(exception.children[0])}</TreeItem>
      );  
    } else {
      return (
        <TreeItem nodeId={uniqueid} label={exception.label} />
      );  
    }
  }

  retrieveSelectedException(exception, selectedId) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }

    if (uniqueid === selectedId) {
      return exception.properties;
    } else if ("children" in exception) {
      return this.retrieveSelectedException(exception.children[0], selectedId);
    } else {
      return [];
    }
  }

  getRootExceptionId(exception) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }
    return uniqueid;
  }

  componentDidMount() {
    if (this.props.exception) {
      var rootExceptionId = this.getRootExceptionId(this.props.exception);
      this.setState({
        expanded: [rootExceptionId],
        selected: [rootExceptionId],
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.exception !== prevProps.exception) {
      var rootExceptionId = this.getRootExceptionId(this.props.exception);
      this.setState({
        expanded: [rootExceptionId],
        selected: [rootExceptionId],
      });
    }
  }

  render() {
    const classes = this.styles;

    const handleToggle = (event, nodeIds) => {
      this.setState({
        expanded: nodeIds,
      });
    };

    const handleSelect = (event, nodeId) => {
      this.setState({
        selected: [nodeId],
      });
    };

    var selectedExceptionProps = [];
    if (this.state.selected.length !== 0) {
      selectedExceptionProps = this.retrieveSelectedException(this.props.exception, this.state.selected[0]);
    };

    return (
      <div id="hierarchy-view-row">
        <div id="hierarchy-view-tree">
          <TreeView
            className={classes.root}
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            expanded={this.state.expanded}
            selected={this.state.selected}
            onNodeToggle={handleToggle}
            onNodeSelect={handleSelect}
          >
            {this.generateTreeItems(this.props.exception)}
          </TreeView>
        </div>
        <div id="hierarchy-view-table">
          <Paper variant="outlined" elevation={0}>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Value</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {selectedExceptionProps.map((row) => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>
                        {(row.name === "message") ? decodeURI(row.value) : row.value}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </div>
      </div>
    );
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class ExceptionViewer extends React.Component {
  exception
  styles

  constructor(props) {
    super(props);
    this.exception = null;
    this.state = {
      error: null,
      isLoaded: false,
      exceptionid: this.props.exceptionid,
      value: 0
    }

    this.styles = makeStyles((theme) => ({
      root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
      },
    }));    
  }

  fetchException() {
    fetch(this.props.spotlighturl + "view?uniqueid=" + this.props.exceptionid)
      .then(res => res.json())
      .then(
        (result) => {
          this.exception = result;
          this.setState({
            error: null,
            isLoaded: true,
            exceptionid: this.props.exceptionid
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.exception = null;
          this.setState({
            error: error,
            isLoaded: true,
            exception: null
          });
        }
      )
  }

  componentDidMount() {
    if (this.props.exceptionid) {
      this.fetchException();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.exceptionid !== prevProps.exceptionid) {
      this.fetchException();
    }
  }

  render() {
    const { error, isLoaded, exceptionid, value } = this.state;
    const classes = this.styles;

    const handleChange = (event, newValue) => {
      this.setState({
        value: newValue
      });
    };

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded && exceptionid) {
      return <div>Loading...</div>;
    } else if (exceptionid){
      return (
        <div className={classes.root}>
          <AppBar position="static">
            <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
              <Tab label="JSON" {...a11yProps(0)} />
              <Tab label="HTML" {...a11yProps(1)} />
              <Tab label="Hierarchy" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <JSONView exception={this.exception} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <HTMLView exception={this.exception} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <HierarchicalView exception={this.exception} />
          </TabPanel>
        </div>
      );
    } else {
      return ( <div></div> );
    }
  }
}

function SeverityIcon(props) {
  return (
    <LensIcon style={{ fontSize: 10, color: props.color }}/>
  );
}

function CellContent(props) {
  if ( props.columnType === "icon" ) {
    var iconColor = null;
    switch(props.value) {
      case "fatal":
        iconColor = 'red';
        break;
      case "error":
        iconColor = 'orange';
        break;
      case "warning":
        iconColor = 'yellow';
        break;
      default:
        iconColor = 'grey';
    }

    return (
      <div class="centered-label">
        <SeverityIcon color={iconColor}/>
      </div>
    );
  } else {
    return (
      <div class="centered-label">
        <span>{props.value}</span>
      </div>
    );
  }
}

class ExceptionList extends React.Component {
  columns
  rows
  styles

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      exceptionList: null,
      page: 0,
      rowsperpage: 10,
      selectedid: null
    }

    this.columns = [
      { id: 'icon', label: '', minWidth: 5 },
      { id: 'time', label: 'Time', minWidth: 170 },
      { id: 'severity', label: 'Severity', minWidth: 20 },
      { id: 'identifier', label: 'Identifier', minWidth: 100 },
      { id: 'message', label: 'Message', minWidth: 300 }
    ];

    this.rows = [];

    this.styles = makeStyles({
      root: {
        width: '100%',
      },
      container: {
        maxHeight: 440,
      },
    }); 
  }

  fetchExceptions() {
    const startTime = this.props.startTime.getTime() / 1000;
    const endTime = this.props.endTime.getTime() / 1000;
    var query = 'select uniqueid, datetime, identifier, occurrences, notifier, severity, message, schema, sessionid, ' +
      'tag, version,class,instance,lid,context,groups,service,zone,uuid, line, module, func, count(*) over() as count ' +
      'from(select catalog.uniqueid, catalog.datetime, catalog.identifier, catalog.occurrences, catalog.notifier, ' +
      'catalog.severity, catalog.message, catalog.schema, catalog.sessionid, catalog.tag, catalog.version, catalog.line, ' +
      'catalog.module,catalog.func,xdaq_application.class,xdaq_application.instance,xdaq_application.lid,' +
      'xdaq_application.context,xdaq_application.groups,xdaq_application.service,xdaq_application.zone,' +
      'xdaq_application.uuid from catalog left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid) ' +
      'where ';

    const filters = this.props.filters.slice();
    for (const filter of filters) {
      if ( (filter.property !== null) && (filter.predicate !== null) && (filter.text !== "") ) {
        switch(filter.predicate) {
          case "contains":
            query += filter.property + " like '%" + filter.text + "%' and ";
            break;
          case "is":
            query += filter.property + " = '" + filter.text + "' and ";
            break;
          case "begins":
            query += filter.property + " like '" + filter.text + "%' and ";
            break;
          case "ends":
            query += filter.property + " like '%" + filter.text + "' and ";
            break;
          default:
        };
      };
    };

    query += 'catalog.dateTime <= ' + endTime + ' and catalog.dateTime >= ' + startTime + ' order by catalog.dateTime ) ' +
    'where rowNum <= 1000';

    fetch(this.props.spotlighturl + "query?statement=" + encodeURI(query))
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            error: null,
            isLoaded: true,
            exceptionList: result,
          });
          if ( result.table.rows[0] && (parseInt(result.table.rows[0].count) >= 1000) ) {
            alert("Retrieved 1000 or more exceptions. Displaying first 1000.");
          };
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            error: error,
            isLoaded: true,
            exceptionList: null,
          });
        }
      )
  }

  StickyHeadTable() {
    const classes = this.styles;
    const selectedID = this.state.selectedid;
  
    const handleChangePage = (event, newPage) => {
      this.setState({
        page: newPage,
      });
    };

    const handleChangeRowsPerPage = (event) => {
      this.setState({
        page: 0,
        rowsperpage: event.target.value
      });
    };

    const handleRowClick = (selectedID) => {
      this.setState({
       selectedid: selectedID
      });
    };

    return (
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader size="small" aria-label="sticky table">
            <TableHead>
              <TableRow>
                {this.columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {this.rows.slice(this.state.page * this.state.rowsperpage, this.state.page * this.state.rowsperpage + this.state.rowsperpage).map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code} onClick={() => {handleRowClick(row.exceptionid);}} selected={selectedID === row.exceptionid}>
                    {this.columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          <CellContent value={column.format && typeof value === 'number' ? column.format(value) : value} columnType={column.id}/>
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={this.rows.length}
          rowsPerPage={this.state.rowsperpage}
          page={this.state.page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    );
  }

  createData(time, identifier, message, uniqueid, severity) {
    //Formatting date
    var date = new Date(time * 1000);
    var readableDate = date.toUTCString();
    //Decoding message
    var decodedMessage = decodeURI(message);
    //Change severity text to lower case
    var lowerCaseSeverity = severity.toLowerCase();
    return ({
      time: readableDate,
      identifier: identifier,
      message: decodedMessage,
      exceptionid: uniqueid,
      severity: lowerCaseSeverity,
      icon: lowerCaseSeverity
    });
  }

  componentDidMount() {
    this.fetchExceptions();
  }

  componentDidUpdate(prevProps) {
    if (this.props.startTime !== prevProps.startTime || this.props.endTime !== prevProps.endTime || this.props.filters !== prevProps.filters) {
      this.fetchExceptions();
    }
  }

  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      this.rows = this.state.exceptionList.table.rows.map((except) => {
        return this.createData(except.dateTime, except.identifier, except.message, except.uniqueid, except.severity);
      });
      return (
        <div>
          <div className="exception-list-div">
            {this.StickyHeadTable()}
          </div>  
          <div className="exception-list-div">
            <ExceptionViewer exceptionid={this.state.selectedid} spotlighturl={this.props.spotlighturl}/>
          </div>       
        </div>
      );
    }
  }
}

class TimeFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startTime: this.props.startTime,
      endTime: this.props.endTime,
    };
  }

  handleStartTimeChange(date) {
    this.setState({
      startTime: date,
    });
    this.props.onStartTimeChange(date);
  }

  handleEndTimeChange(date) {
    this.setState({
      endTime: date,
    });
    this.props.onEndTimeChange(date);
  }

  render () {
    return (
      <div>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Grid container>
            <div id="start-date-div">
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                label="Start date"
                format="yyyy-MM-dd"
                value={this.state.startTime}
                onChange={(date) => this.handleStartTimeChange(date)}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </div>
            <div id="start-time-div">
              <KeyboardTimePicker
                margin="normal"
                id="time-picker"
                label="Start time"
                value={this.state.startTime}
                onChange={(date) => this.handleStartTimeChange(date)}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </div>
            <div id="end-date-div">
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                label="End date"
                format="yyyy-MM-dd"
                value={this.state.endTime}
                onChange={(date) => this.handleEndTimeChange(date)}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </div>
            <div id="end-time-div">
              <KeyboardTimePicker
                margin="normal"
                id="time-picker"
                label="End time"
                value={this.state.endTime}
                onChange={(date) => this.handleEndTimeChange(date)}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </div>
          </Grid>
        </MuiPickersUtilsProvider>
      </div>
    );
  }
}

class PropertyFilter extends React.Component {
  render() {
    const propertyList = [
      { title: 'Identifier', name: 'catalog.identifier' },
      { title: 'Severity', name: 'catalog.severity' },
      { title: 'Occurrences', name: 'catalog.occurrences' },
      { title: 'Notifier', name: 'catalog.notifier' },
      { title: 'Exception UUID', name: 'catalog.uniqueid' },
      { title: 'Message', name: 'catalog.message' },
      { title: 'Schema', name: 'catalog.schema' },
      { title: 'Session ID', name: 'catalog.sessionid' },
      { title: 'Tag', name: 'catalog.tag' },
      { title: 'Version', name: 'catalog.version' },
      { title: 'Instance', name: 'xdaq_application.instance' },
      { title: 'Local ID', name: 'xdaq_application.lid' },
      { title: 'Context', name: 'xdaq_application.context' },
      { title: 'Group', name: 'xdaq_application.groups' },
      { title: 'Service', name: 'xdaq_application.service' },
      { title: 'Zone', name: 'xdaq_application.zone' },
      { title: 'Application UUID', name: 'xdaq_application.uuid' },
    ];

    const predicateList = [
      { title: 'contains', name: 'contains' },
      { title: 'is', name: 'is' },
      { title: 'begins with', name: 'begins' },
      { title: 'ends with', name: 'ends' },
    ];

    return (
      <Grid container>
        <div className="prop-filter-div">
          <Autocomplete
            id="property-combo"
            size="small"
            options={propertyList}
            getOptionLabel={(option) => option.title}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Property" variant="outlined" />}
            onChange={(event, value, reason) => this.props.onPropertyChange(event, value, reason)}
          />
        </div>
        <div className="prop-filter-div">
          <Autocomplete
            id="predicate-combo"
            size="small"
            options={predicateList}
            getOptionLabel={(option) => option.title}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Predicate" variant="outlined" />}
            onChange={(event, value, reason) => this.props.onPredicateChange(event, value, reason)}
          />
        </div>
        <div className="prop-filter-div">
          <TextField
            label="Text"
            size="small"
            variant="outlined"
            style={{ width: 300 }}
            onChange={(event) => this.props.onTextChange(event)}
          />
        </div>
        <div className="prop-filter-div">
          <Button
            variant="contained"
            disabled={this.props.addDisabled}
            onClick={() => this.props.onAddClick()}
          >
            +
          </Button>
        </div>
        <div className="prop-filter-div">
          <Button
            variant="contained"
            disabled={this.props.removeDisabled}
            onClick={() => this.props.onRemoveClick()}
          >
            -
          </Button>
        </div>
      </Grid>
    );
  }
}

function ColdspotLogo(props) {
  return (
    <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <section style={{ color: blue[500], fontSize: 20, fontWeight: 'bold', fontVariant: [ 'small-caps' ] }}>
          coldspot
        </section>
        <AcUnitIcon style={{ fontSize: 50, color: blue[500] }}/>
      </div>
    </div>
  );
}

class ColdSpot extends React.Component {
  startTime
  endTime
  filters
  spotlighturl

  constructor(props) {
    super(props);
    this.startTime = new Date();
    this.endTime = new Date();
    this.filters = [
      {
        property: null,
        predicate: null,
        text: "",
        addDisabled: false,
        removeDisabled: true,
      }
    ];

    this.state = {
      startTime: this.startTime,
      endTime: this.endTime,
      filters: this.filters,
    };

    const params = queryString.parse(this.props.location.search);
    const value = params.spotlighturl;
    if (typeof(value) !== "undefined" && value) {
      this.spotlighturl = value;
    } else {
      this.spotlighturl = "";
    }
  }

  handleStartTimeChange(date) {
    this.startTime = date;
  }

  handleEndTimeChange(date) {
    this.endTime = date;
  }

  handlePropertyChange(event, value, reason, index) {
    if ( reason === "clear" ) {
      this.filters[index].property = null;
    } else {
      this.filters[index].property = value.name;
    }
  }

  handlePredicateChange(event, value, reason, index) {
    if ( reason === "clear" ) {
      this.filters[index].predicate = null;
    } else {
      this.filters[index].predicate = value.name;
    }
  }

  handleTextChange(event, index) {
    this.filters[index].text = event.target.value;
  }

  handleAddClick(index) {
    this.filters[index].addDisabled = true;
    this.filters[index].removeDisabled = true;
    this.filters.push(
      {
        property: null,
        predicate: null,
        text: "",
        addDisabled: false,
        removeDisabled: false,
      }
    );
    this.setState({
      filters: this.filters.slice(),
    });
  }

  handleRemoveClick(index) {
    this.filters[index - 1].addDisabled = false;
    if ( (index - 1) === 0 ) {
      this.filters[index - 1].removeDisabled = true;
    } else {
      this.filters[index - 1].removeDisabled = false;
    }
    this.filters.pop();
    this.setState({
      filters: this.filters.slice(),
    });
  }

  handleSearchClick() {
    this.setState({
      startTime: this.startTime,
      endTime: this.endTime,
      filters: this.filters.slice(),
    });
  }

  render() {
    const filters = this.state.filters.map((elem, index) => {
      return (
        <div className="coldspot-div">
          <PropertyFilter
            className="property-filter"
            onPropertyChange={(event, value, reason) => this.handlePropertyChange(event, value, reason, index)}
            onPredicateChange={(event, value, reason) => this.handlePredicateChange(event, value, reason, index)}
            onTextChange={(event) => this.handleTextChange(event, index)}
            onAddClick={() => this.handleAddClick(index)}
            onRemoveClick={() => this.handleRemoveClick(index)}
            addDisabled={elem.addDisabled}
            removeDisabled={elem.removeDisabled}
          />
        </div>
      );
    });

    var exceptionList;
    exceptionList = (
      <ExceptionList
        spotlighturl={this.spotlighturl}
        startTime={this.state.startTime}
        endTime={this.state.endTime}
        filters={this.state.filters.slice()}
      />
    );

    return (
      <div>
        <div>
          <ColdspotLogo/>
        </div>
        <div className="coldspot-div">
          <TimeFilter
            startTime={this.state.startTime}
            endTime={this.state.endTime}
            onStartTimeChange={(date) => this.handleStartTimeChange(date)}
            onEndTimeChange={(date) => this.handleEndTimeChange(date)}
          />
        </div>
        {filters}
        <div className="coldspot-div">
          <Button 
            variant="contained"
            size="small"
            onClick={() => this.handleSearchClick()}
          >
            Search
          </Button>
        </div>
        {exceptionList}
      </div>
    );
  }
}

const ColdSpotWithRouter = withRouter(ColdSpot);

ReactDOM.render(
  <BrowserRouter>
    <Route path="/" component={ ColdSpotWithRouter }/>
   </BrowserRouter>,
  document.getElementById('root')
);
