#!/bin/bash

HELM_FILE=$1

ZONES=(daq3val minidaq emu muondt ecal es hcal bril pixel rpc tracker trigger tcdsd3v tcdslight tcdsp5 totem ctpps mtd zdc)
for ZONE in "${ZONES[@]}";
do
  helm install --set zone.name=$ZONE $ZONE-hotspot $HELM_FILE;
done

helm install --set zone.name=cdaq3 --set zone.application.replicas=4 cdaq3-hotspot $HELM_FILE
