#!/bin/bash

type=
while getopts t:r: flag
do
  case "${flag}" in
    t) type=${OPTARG};;
    r) replicas=${OPTARG};;
  esac
done

echo "type="$type
echo "replicas="$replicas

if [[ $type == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

if [[ $replicas == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

rm output.txt
touch output.txt
testcount=5
#array=(13 21 23 40)
#for i in "${array[@]}"
for (( i=1; i<=$replicas; i++ ))
do
  timesum=0
  mintime=100000
  maxtime=0
  for (( j=1; j<=$testcount; j++ ))
  do
    echo "i="$i
    #Uninstalling benchmark chart
    helm uninstall cmsos-kube-helm-benchmark
    kubectl get namespaces
    kubectl wait --for=delete namespace/benchmark --timeout=600s
    echo "benchmark namespace deleted"
    kubectl get namespaces

    case "$type" in
      clean) wassh_hosts 'sudo docker rmi gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
      preloaded) wassh_hosts 'sudo docker pull gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
      skip) ;;
    esac

    start_time=$(date +%s)

    helm install --set zone.name=benchmark,zone.application.replicas=$i cmsos-kube-helm-benchmark chart/cmsos-kube-helm-benchmark-1.0.0.tgz

    kubectl get pods -l application=runner -n benchmark
    kubectl wait pod -l application=runner -n benchmark --for condition=ready --timeout=3600s

    end_time=$(date +%s)

    kubectl get pods -l application=runner -n benchmark

    elapsed=$((end_time - start_time))
    ((timesum += elapsed))
    mintime=$(($elapsed<$mintime ? $elapsed : $mintime))
    maxtime=$(($elapsed>$maxtime ? $elapsed : $maxtime))

    echo "number of replicas = "$i" time elapsed="$elapsed"s"
  done
  avgtime=$(jq -n $timesum/$testcount)
  echo $i $avgtime $mintime $maxtime >> output.txt
done
