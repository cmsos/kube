from string import Template
import http.client
from urllib.parse import urlparse
from xml.etree import ElementTree
import sys

def hasfault(dom):
    names = dom.findall('.//ns:Fault',{'ns': 'http://schemas.xmlsoap.org/soap/envelope/'})
    if names:
        #print("found fault in response" + str(names))
        return True
    return False


# SOAP commands


def parameter_set(url,classname,instance,pname,ptype,value):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    bodyt = Template("""<?xml version="1.0" encoding="UTF-8"?>
             <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
                <SOAP-ENV:Body>
                <xdaq:ParameterSet xmlns:xdaq="urn:xdaq-soap:3.0">
                    <p:properties xmlns:p="urn:xdaq-application:$classname" xsi:type="soapenc:Struct">
                        <p:$pname xsi:type="$ptype">$value</p:$pname>
                    </p:properties>
                </xdaq:ParameterSet>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>""")
    body= bodyt.substitute(value=value,classname=classname,pname=pname,ptype=ptype)
    print("parameter_set: url = " + url + ", classname = " + classname + ", instance = " + str(instance))
    # print(url)
    #print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        replymessage = str(response.read().decode());
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        #print(replymessage);
        #print(response.status, response.reason)
        conn.close()
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )
    except Fault as ex:
        raise ex


def parameter_get(url, classname, instance, pname, ptype):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    bodyt = Template("""<?xml version="1.0" encoding="UTF-8"?>
             <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
                <SOAP-ENV:Body>
                <xdaq:ParameterGet xmlns:xdaq="urn:xdaq-soap:3.0">
                    <p:properties xmlns:p="urn:xdaq-application:$classname" xsi:type="soapenc:Struct">
                        <p:$pname xsi:type="$ptype"/>
                    </p:properties>
                </xdaq:ParameterGet>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>""")
    body = bodyt.substitute(classname=classname,pname=pname,ptype=ptype)
    print("parameter_get: url = " + url + ", classname = " + classname + ", instance = " + str(instance) + ", pname = " + pname + ", ptype = " + ptype)
    # print(url)
    #print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        # print(response.status, response.reason)
        #print(response.read().decode())
        # https://serverfault.com/questions/967789/python-parsing-http-soap-response
        namespaces = {'p': 'urn:xdaq-application:' + classname}
        replymessage = str(response.read().decode());
        #print(replymessage);
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        names = dom.findall('.//p:' + pname, namespaces)
        for name in names:
            #print("found value in response" + str(name.text))
            return str(name.text)
        conn.close()
    except Exception as ex:
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )
        #sys.exit(3)
    except Fault as ex:
        raise ex
        #sys.exit(3)


def configure(url,classname,instance):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    body = """<?xml version="1.0" encoding="UTF-8"?>
              <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
            <SOAP-ENV:Body>
            <xdaq:Configure xmlns:xdaq="urn:xdaq-soap:3.0">
            </xdaq:Configure>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>"""
    print("configure: url = " + url + ", classname = " + classname + ", instance = " + str(instance))
    # print(url)
    # print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        replymessage = str(response.read().decode());
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        #print(replymessage);
        # print(response.status, response.reason)
        conn.close()
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )

def enable(url,classname,instance):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    body = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
            <SOAP-ENV:Body>
        <xdaq:Enable xmlns:xdaq="urn:xdaq-soap:3.0">
        </xdaq:Enable>
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>"""
    print("enable: url = " + url + ", classname = " + classname + ", instance = " + str(instance))
    # print(url)
    # print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        # print(response.status, response.reason)
        replymessage = str(response.read().decode());
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        #print(replymessage);
        conn.close()
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )
    except Fault as ex:
        raise ex

def halt(url,classname,instance):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    body = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
            <SOAP-ENV:Body>
        <xdaq:Halt xmlns:xdaq="urn:xdaq-soap:3.0">
        </xdaq:Halt>
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>"""
    print("halt: url = " + url + ", classname = " + classname + ", instance = " + str(instance))
    # print(url)
    # print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        replymessage = str(response.read().decode());
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        #print(replymessage);
        # print(response.status, response.reason)
        conn.close()
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex)  )
        #print("Error: %s" % ex)
        #sys.exit(3)
    except Fault as ex:
        raise ex

def connect(url,classname,instance):
    headers = {'SOAPAction': 'urn:xdaq-application:class=' + classname + ',instance=' + str(instance), 'content-type': 'application/soap+xml'}
    body = """<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemasxmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
                <SOAP-ENV:Header>
                </SOAP-ENV:Header>
            <SOAP-ENV:Body>
        <xdaq:Connect xmlns:xdaq="urn:xdaq-soap:3.0">
        </xdaq:Connect>
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>"""
    print("connect: url = " + url + ", classname = " + classname + ", instance = " + str(instance))
    # print(url)
    # print(body)
    # print(headers)
    try:
        conn = http.client.HTTPConnection(urlparse(url).hostname, urlparse(url).port)
        conn.request("POST", "", body, headers)
        response = conn.getresponse()
        replymessage = str(response.read().decode());
        dom = ElementTree.fromstring(replymessage)
        if hasfault(dom):
            print("-----> GOT FAULT " + replymessage);
            raise Fault("Failed soap to " + url + " with fault " + str(replymessage))
        #print(replymessage);
        # print(response.status, response.reason)
        conn.close()
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed connection to " + url + " with error " + str(ex))
        #print("Error: %s" % ex)
        #sys.exit(3)
    except Fault as ex:
        raise ex
