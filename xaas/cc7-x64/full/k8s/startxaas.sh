#!/bin/bash

export KUBECONFIG=~/.kube/config
kubectl create -f ns.yaml
kubectl create -f service.yaml
sleep 10
kubectl create -f busdeploy.yaml
sleep 20
kubectl create -f deployment.yaml
