#!/bin/bash

daq2_hosts_12="ru-c2e12-10-01 ru-c2e12-11-01 ru-c2e12-12-01 ru-c2e12-13-01 ru-c2e12-14-01 ru-c2e12-15-01 ru-c2e12-16-01 ru-c2e12-17-01 ru-c2e12-18-01 ru-c2e12-19-01 ru-c2e12-22-01 ru-c2e12-23-01 ru-c2e12-24-01 ru-c2e12-25-01 ru-c2e12-26-01 ru-c2e12-27-01 ru-c2e12-28-01 ru-c2e12-29-01 ru-c2e12-30-01 ru-c2e12-31-01 ru-c2e12-34-01 ru-c2e12-35-01 ru-c2e12-36-01 ru-c2e12-37-01 ru-c2e12-38-01 ru-c2e12-39-01"
daq2_hosts_13="ru-c2e13-10-01 ru-c2e13-11-01 ru-c2e13-12-01 ru-c2e13-13-01 ru-c2e13-14-01 ru-c2e13-15-01 ru-c2e13-16-01 ru-c2e13-17-01 ru-c2e13-18-01 ru-c2e13-19-01 ru-c2e13-22-01 ru-c2e13-23-01 ru-c2e13-24-01 ru-c2e13-25-01 ru-c2e13-26-01 ru-c2e13-27-01 ru-c2e13-28-01 ru-c2e13-29-01 ru-c2e13-30-01 ru-c2e13-31-01 ru-c2e13-34-01 ru-c2e13-35-01 ru-c2e13-36-01 ru-c2e13-38-01 ru-c2e13-39-01 ru-c2e13-40-01"
daq2_hosts_14="ru-c2e14-10-01 ru-c2e14-11-01 ru-c2e14-12-01 ru-c2e14-13-01 ru-c2e14-14-01 ru-c2e14-15-01 ru-c2e14-16-01 ru-c2e14-17-01 ru-c2e14-18-01 ru-c2e14-19-01 ru-c2e14-22-01 ru-c2e14-23-01 ru-c2e14-24-01 ru-c2e14-25-01 ru-c2e14-26-01 ru-c2e14-27-01 ru-c2e14-28-01 ru-c2e14-29-01 ru-c2e14-30-01 ru-c2e14-31-01 ru-c2e14-34-01 ru-c2e14-35-01 ru-c2e14-36-01 ru-c2e14-37-01 ru-c2e14-38-01 ru-c2e14-39-01 ru-c2e14-40-01"
daq2_hosts_15="ru-c2e15-10-01 ru-c2e15-11-01 ru-c2e15-12-01 ru-c2e15-13-01 ru-c2e15-14-01 ru-c2e15-15-01 ru-c2e15-16-01 ru-c2e15-17-01 ru-c2e15-18-01 ru-c2e15-19-01 ru-c2e15-22-01 ru-c2e15-23-01 ru-c2e15-24-01 ru-c2e15-25-01 ru-c2e15-26-01 ru-c2e15-27-01 ru-c2e15-28-01 ru-c2e15-29-01 ru-c2e15-30-01 ru-c2e15-31-01 ru-c2e15-34-01 ru-c2e15-35-01 ru-c2e15-36-01 ru-c2e15-37-01 ru-c2e15-38-01 ru-c2e15-39-01 ru-c2e15-40-01"
daq2_clients=$daq2_hosts_12" "$daq2_hosts_14
daq2_servers=$daq2_hosts_13" "$daq2_hosts_15
daq2_hosts=$daq2_hosts_12" "$daq2_hosts_13" "$daq2_hosts_14" "$daq2_hosts_15

for val in $daq2_hosts; do
  echo ssh $val \"$1\"
  ssh $val "$1"
done
