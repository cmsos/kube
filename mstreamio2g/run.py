#!/usr/bin/python3

import time
import getopt, sys
import soap
import measure

#Args used to parameterize 
numreplicas=1
port=0
domainname=""
fragmentsizes = [128,256,512,1000,2000,4000,8000,12000,16000,20000,24000,32000,64000,128000,256000]


full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:p:s:d:"
long_options = ["help", "replicas=", "port=", "sizes=", "domainname="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: run.py [-r #,--replicas=#] [-p # ,--port=#] [-s # ,--sizes='s [s ...]'] [-d <name> ,--domainname=<name>]")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-s", "--sizes"):
        #fragmentsizes = current_value.split()
        fragmentsizes = [int(i) for i in current_value.split()]
    elif current_argument in ("-d", "--domainname"):
        domainname = current_value
    else:
        print ("Invalid option")
        sys.exit(2)

#

print("Wait for mstreamio2g to be ready...")
expected = numreplicas*2
connected = False
while not connected:
    total = 0
    try:
        for r in range(numreplicas):
            c = soap.parameter_get("http://client-"+str(r)+".client" + domainname + ":"+str(port), "Client", r, "liveness", "xsd:boolean")
            #print ("Return value from parameter get is " + c)
            if (c == 'true'):
                total += 1
            c = soap.parameter_get("http://server-"+str(r)+".server" + domainname + ":"+str(port), "Server", r, "liveness", "xsd:boolean")
            #print ("Return value from parameter get is " + c)
            if (c == 'true'):
                total += 1
    except Exception as ex:
        print(ex)
    except Fault as ex:
        print(ex)

    if (total == expected):
        connected = True
    else:
        time.sleep(1)
    print("Found " + str(total) + " nodes out of " + str(expected))
print("done livness.")
f = open("/tmp/runner-alive", "x")
f.close()
print("Skipping measurements. Exiting...")
sys.exit(0)

try:

    #Run gevb for N replicas
    for fsize in fragmentsizes:
        print("Measure input size " + str(fsize))
        measurements = {}
        measurements['measurement'] = []
        measurements['fragSizeRMS'] = 0
        measurements['fragSize'] = fsize
    
        #  setup
        for r in range(numreplicas):
            soap.parameter_set("http://client-" + str(r) + ".client" + domainname + ":" + str(port), "Client", r, "currentSize", "xsd:unsignedLong", fsize)
            soap.parameter_set("http://server-" + str(r) + ".server" + domainname + ":" + str(port), "Server", r, "currentSize", "xsd:unsignedLong", fsize)
    
        time.sleep(5)
       
        # start
        for r in range(numreplicas):
            soap.start("http://server-"+str(r)+".server" + domainname + ":"+str(port), "Server", r)
    
        # bu enable
        for r in range(numreplicas):
            soap.start("http://client-"+str(r)+".client" + domainname + ":"+str(port), "Client", r)

        #-----------------------------------
        ticking = False
        while not ticking:
            total = 0
            try:
                for r in range(numreplicas):
                    c1 = soap.parameter_get("http://client-"+str(r)+".client" + domainname + ":"+str(port), "Client", r, "counter", "xsd:unsignedLong")
                    c2 = soap.parameter_get("http://client-"+str(r)+".client" + domainname + ":"+str(port), "Client", r, "counter", "xsd:unsignedLong")
                    c = int(c2) - int(c1)
                    #print ("Return value from parameter get is " + c)
                    if (c != 0):
                        total += 1
                    c1 = soap.parameter_get("http://server-"+str(r)+".server" + domainname + ":"+str(port), "Server", r, "counter", "xsd:unsignedLong")
                    c2 = soap.parameter_get("http://server-"+str(r)+".server" + domainname + ":"+str(port), "Server", r, "counter", "xsd:unsignedLong")
                    c = int(c2) - int(c1)
                    #print ("Return value from parameter get is " + c)
                    if (c != 0):
                        total += 1
            except Exception as ex:
                print(ex)
            except Fault as ex:
                print(ex)

            if (total == expected):
                ticking = True
                f = open("/tmp/runner-alive", "x")
                f.close()

            print("Found " + str(total) + " ticking nodes out of " + str(expected))
            sys.stdout.flush()
            time.sleep(5)
        print("done ticking test.")
        #-----------------------------------

        time.sleep(60)

        # stop
        for r in range(numreplicas):
            soap.stop("http://client-"+str(r)+".client" + domainname + ":"+str(port), "Client", r)
            soap.stop("http://server-"+str(r)+".server" + domainname + ":"+str(port), "Server", r)
    
        time.sleep(5)

except Exception as ex:
    print(ex)
except Fault as ex:
    print(ex)

# end of tests

print("done")
