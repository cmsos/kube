var DataBaseFactory = require('./db.js');
var dbfactory = new DataBaseFactory();
var zerot = new Date(0);

var fs = require('fs'),
xml2js = require('xml2js');
var stripNS = require('xml2js').processors.stripPrefix
var parser = new xml2js.Parser({ trim: true, explicitArray: false, tagNameProcessors: [stripNS] });

let db;
let DatabaseConnection = async function connect(user,pass,tns) {
	db = await dbfactory.create(user, pass, tns);
}

var express = require('express');
var app = express();
bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.all('*', function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
});
	//oout->getHTTPResponseHeader().addHeader("Expires", "0");
		//out->getHTTPResponseHeader().addHeader("Cache-Control",
						 //"no-store, no-cache, must-revalidate, max-age=0");
		//out->getHTTPResponseHeader().addHeader("Cache-Control",
						 //"post-check=0, pre-check=0");
		//out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");


app.get( "/crossdomain.xml", onCrossDomainHandler )
function onCrossDomainHandler( req, res ) {
	var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM' + 
		' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n';
	xml += '<allow-access-from domain="*" to-ports="*"/>\n';
	xml += '</cross-domain-policy>\n';

	req.setEncoding('utf8');
	res.writeHead( 200, {'Content-Type': 'text/xml'} );
	res.end( xml );
}

app.get(['/urn:xdaq-application:service=sentinelspotlight2g/query','/query'], async (req, res) => {
	let statement = req.query.statement;
	if (statement === undefined )
	{
		res.statusCode = 404;
		res.setHeader('Content-Type', 'text/plain');
		res.end('Missing statement paramater');
		return;
	}

	//const user = await User.findOne({email: req.body.email})
	res.setHeader("Content-Type", "application/json");
	const data = await db.query(statement);
	res.writeHead(200);
	res.end(JSON.stringify(data));
});

function isNumber(value) {
	if ((undefined === value) || (null === value)) {
		return false;
	}
	if (typeof value == 'number') {
		return true;
	}
	return !isNaN(value - 0);
}

app.get(['/urn:xdaq-application:service=sentinelspotlight2g/lastStoredEvents','/lastStoredEvents'], async (req, res) => {
	res.setHeader("Content-Type", "application/json");

	// get current time
	var currenttime = new Date();
	var since = currenttime;

	if (typeof req.query.start !== 'undefined') {
		let start = new Date(req.query.start);
		//console.log("parsed  start is "+ req.query.start + " while URL start is " + req.query.start );
		//console.log("converte start is "+ start.toISOString() + " while zerot is " + zerot.toISOString());
		if ( start.toISOString() != zerot.toISOString()) {
			since = start;
		}
	}
	//console.log("spotlight.since is "+ since);
	const data = await db.lastStoredEvents(since);

	//console.log("lastStoreTime was "+ data.lastStoreTime  + " and zero is " + zerot.toISOString());
	if (data.lastStoreTime == zerot.toISOString()  ) {
		//console.log("lastStoreTime was 0 therefore set to since "+ JSON.stringify(data));
		data.lastStoreTime = since.toISOString();
	}
	//console.log("response data is "+ JSON.stringify(data));
	//console.log("last stored time is "+ data.lastStoreTime );
	res.writeHead(200);
	res.end(JSON.stringify(data));
});

app.get(['/urn:xdaq-application:service=sentinelspotlight2g/view','/view'], async (req, res) => {
	let uniqueid = req.query.uniqueid;
	if (uniqueid === undefined )
	{
		res.statusCode = 404;
		res.setHeader('Content-Type', 'text/plain');
		res.end('Missing uniqueid paramater');
		return;
	}

	res.setHeader("Content-Type", "application/json");
	const blob = await db.view(uniqueid);
	res.writeHead(200);
	res.end(JSON.stringify(blob));
});

app.post(['/urn:xdaq-application:service=sentinelspotlight2g/rearm','/rearm'], async (req, res) => {
	//console.log(req.body);

	var uuids = [];
	var requests = JSON.parse(JSON.stringify(req.body.exception));

	if ( Array.isArray(requests) ) {
		for (i in requests) {
			//console.log("my request is "+ requests[i]);
			uuids.push(requests[i]);
		} 
	} else {
		uuids.push(requests); // a single uuid string
	}

		res.setHeader("Content-Type", "application/json");
		response = await db.rearm(uuids,"uknown user"); // to know user we need to implement authentication support
		res.writeHead(200);
		res.end();
});

app.get(['/urn:xdaq-application:service=sentinelspotlight2g/rearm','/rearm'], async (req, res) => {
	let uniqueid = req.query.exception;
	var uuids = [];
	uuids.push(uniqueid); // a single uuid string
	res.setHeader("Content-Type", "application/json");
	response = await db.rearm(uuids, "unknown user"); // to know user we need to implement authentication support
	res.writeHead(200);
	res.end();
});

app.get(['/ping'], async (req, res) => {
	res.setHeader("Content-Type", "application/json");
	const data = await db.ping();
	res.writeHead(200);
	res.end(JSON.stringify(data));
});

module.exports = class Spotlight {
	create(oclfile, zonename) {
		fs.readFile(oclfile, function(err, data) {
			parser.parseString(data, function (err, result) {
				var zones = result['OracleDBCredentials']['XaasZone'];
				for(var i = 0; i < zones.length; i++) {
					var zone = zones[i]['$']['zone'];
					if ( zone === zonename ) {
						console.log("Found zone " + zone);
						var user = zones[i]['databaseUser'];
						var pass = zones[i]['databasePassword'];
						var tns = zones[i]['databaseTnsName'];

						console.log("Before connecting");
						let connection = DatabaseConnection(user,pass,tns);

						connection.then(function(result) {
							console.log("Oracle database is connected")
						});
						return;
					}
				}
			});
		});
		return app;
	}
}
