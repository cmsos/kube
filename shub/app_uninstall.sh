#!/bin/bash

type=
while getopts t:r: flag
do
  case "${flag}" in
    t) type=${OPTARG};;
    r) replicas=${OPTARG};;
  esac
done

echo "type="$type
echo "replicas="$replicas

if [[ $type == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

if [[ $replicas == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

rm output.txt
touch output.txt

#Uninstalling shub chart
helm uninstall cmsos-kube-helm-shub
kubectl get namespaces
kubectl wait --for=delete namespace/shub --timeout=600s
echo "shub namespace deleted"
kubectl get namespaces

testcount=5
#array=(2 20 40 60 80 100 106)
#for i in "${array[@]}"
for (( i=1; i<=$replicas; i++ ))
do
  timesum=0
  mintime=100000
  maxtime=0
  for (( j=1; j<=$testcount; j++ ))
  do
    echo "i="$i
    case "$type" in
      clean) wassh_hosts 'sudo docker rmi gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.1.0.0';;
      preloaded) wassh_hosts 'sudo docker pull gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.1.0.0';;
      skip) ;;
    esac

    helm install --set zone.name=shub,zone.application.replicas=$i cmsos-kube-helm-shub chart/cmsos-kube-helm-shub-1.0.0.tgz

    runnercount=0
    until [ "$runnercount" -eq "1" ];
    do
      runnercount=$(kubectl get deployment runner -n shub -o=jsonpath='{.status.readyReplicas}')
      if [ -z "$runnercount" ]
      then
        runnercount=0
      fi
    done;

    start_time=$(date +%s)

    #Uninstalling shub chart
    helm uninstall cmsos-kube-helm-shub
    kubectl get namespaces
    kubectl wait --for=delete namespace/shub --timeout=600s

    end_time=$(date +%s)

    echo "shub namespace deleted"
    kubectl get namespaces

    kubectl get pods -l application=runner -n shub

    elapsed=$((end_time - start_time))
    ((timesum += elapsed))
    mintime=$(($elapsed<$mintime ? $elapsed : $mintime))
    maxtime=$(($elapsed>$maxtime ? $elapsed : $maxtime))

    echo "number of replicas = "$i" time elapsed="$elapsed"s"
  done
  avgtime=$(jq -n $timesum/$testcount)
  echo $i $avgtime $mintime $maxtime >> output.txt
done
