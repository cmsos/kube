import operator

from TestCase import TestCase
from Context import RU,BU

class case_2x1_EVMmetaData(TestCase):

    def runTest(self):
        self.configureEvB(maxTries=20)
        self.enableEvB()
        # Remove size the FED 1022
        #self.checkEVM(6296)
        self.checkEVM(6144)
        self.checkRU(24576)
        #self.checkBU(30872)
        self.checkBU(30720)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU")
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',0,operator.eq,"BU")
        self.stopEvB()
        self.haltEvB()


    def fillConfiguration(self,symbolMap):
        self._config.add( RU(symbolMap,[
             ('inputSource','string','Local'),
             # Removed pseudo FED till the MetaDataRetrieverDIPBridge works
             #('fedSourceIds','unsignedInt',(512,25,17,1022))
             ('fedSourceIds','unsignedInt',(512,25,17))
            ]) )
        self._config.add( RU(symbolMap,[
             ('inputSource','string','Local'),
             ('fedSourceIds','unsignedInt',list(range(1,13)))
            ]) )
        self._config.add( BU(symbolMap,[
             ('dropEventData','boolean','true'),
             ('lumiSectionTimeout','unsignedInt','0')
            ]) )
