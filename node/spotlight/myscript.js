// myscript.js
// This example uses Node 8's async/await syntax.

var DataBaseFactory = require('./db.js');
var dbfactory = new DataBaseFactory();

var currenttime =  new Date();
var since = currenttime.getTime() / 1000;

let db;
let DatabaseConnection = async function connect() {
	db = await dbfactory.create("CMS_HCAL_ERR_LOG", "************", "cms_omds_lb");
}

let connection = DatabaseConnection();

connection.then(function(result) {
   console.log(" data base is connected")
})


// test call back for DB select
async function run(db) {
	try {
		const data = await db.lastStoredEvents(since);
		console.log("data is "+ JSON.stringify(data));
		console.log("last stored time is "+ data.lastStoreTime );
		if (data.lastStoreTime > since )
			since = data.lastStoreTime;

		//const counter = await db.query(`select count(*) from catalog`);
		//console.log("counter is "+ JSON.stringify(counter));

		console.log("Found num " +data.table.rows.length);
		if (data.table.rows.length > 0) {
			const bloba = await db.view(data.table.rows[0].exception);
			console.log("blob for "+ data.table.rows[0].exception + " is " + JSON.stringify(bloba));
		}

		//const blobb = await db.view('c43527b5-37e5-49bd-afa6-ec34d9743bd1');
		//console.log("blob is  " + JSON.stringify(blobb));
	} catch (err) {
    	console.error(err);
  	}
}


let i = 0;
setInterval(() => {
    console.log('Infinite Loop Test interval n:', i++);
    run(db);
}, 2000)
