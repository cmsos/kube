import TestCase

class ConfigCase(TestCase):

    def __init__(self,config,stdout,fedSizeScaleFactors,defaultFedSize,afterStartupCallback=None,waitForStateTransitions=False):
        TestCase.__init__(self,config,stdout,afterStartupCallback,waitForStateTransitions)
        self.fedSizeScaleFactors = fedSizeScaleFactors
        self.defaultFedSize = defaultFedSize


    def destroy(self):
        TestCase.destroy(self)


    def calculateFedSize(self,fedId,fragSize,fragSizeRMS):
        if len(self.fedSizeScaleFactors):
            try:
                (a,b,c,d,rms) = self.fedSizeScaleFactors[fedId]
                relSize = fragSize / self.defaultFedSize
                fedSize = a + b*relSize + c*relSize*relSize + d*relSize*relSize*relSize
                fedSizeRMS = fedSize * rms
                return (int(fedSize+4)&~0x7,int(fedSizeRMS+4)&~0x7)
            except KeyError:
                if fedId not in ('0xffffffff','9999'):
                    print("Missing scale factor for FED id "+fedId)
        return TestCase.calculateFedSize(self,fedId,fragSize,fragSizeRMS)
