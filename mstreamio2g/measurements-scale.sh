#!/bin/bash

type=
while getopts t:r: flag
do
  case "${flag}" in
    t) type=${OPTARG};;
    r) replicas=${OPTARG};;
  esac
done

echo "type="$type
echo "replicas="$replicas

if [[ $type == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

if [[ $replicas == '' ]]
then
  echo 'Usage:'
  echo './measurements.sh -t (clean|preloaded|skip) -r <replicas>'
  exit 0
fi

rm output.txt
touch output.txt

  #Uninstalling mstreamio2g chart
  helm uninstall cmsos-kube-helm-mstreamio2g
  kubectl get namespaces
  kubectl wait --for=delete namespace/mstreamio2g --timeout=600s
  echo "mstreamio2g namespace deleted"
  kubectl get namespaces
  helm install --set zone.name=mstreamio2g,zone.application.replicas=1 cmsos-kube-helm-mstreamio2g chart/cmsos-kube-helm-mstreamio2g-3.0.0.tgz

  kubectl get pods -l application=runner -n mstreamio2g
  kubectl wait pod -l application=runner -n mstreamio2g --for condition=ready --timeout=3600s
  kubectl get pods -l application=runner -n mstreamio2g

for (( i=1; i<=$replicas; i++ ))
do
  echo "i="$i

  case "$type" in
    clean) wasshru 'sudo docker rmi gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
    preloaded) wasshru 'sudo docker pull gitlab-registry.cern.ch/cmsos/docker/docker-image-cmsos-master-cc8-x64-fullofed49lts:1.0.0.0';;
    skip) ;;
  esac

  #Scaling to 0
  kubectl scale statefulsets client -n mstreamio2g --replicas=0
  kubectl scale statefulsets server -n mstreamio2g --replicas=0
  kubectl scale deployment runner -n mstreamio2g --replicas=0
  kubectl get pods -n mstreamio2g
  kubectl wait --for=delete pod -l application=runner -n mstreamio2g --timeout=600s
  kubectl get pods -n mstreamio2g
  kubectl wait --for=delete pod -l application=client -n mstreamio2g --timeout=600s
  kubectl wait --for=delete pod -l application=server -n mstreamio2g --timeout=600s
  kubectl get pods -n mstreamio2g

  start_time=$(date +%s)

  #Scaling to "i"
  kubectl scale statefulsets client -n mstreamio2g --replicas=$i
  kubectl scale statefulsets server -n mstreamio2g --replicas=$i
  kubectl scale deployment runner -n mstreamio2g --replicas=1

  kubectl get pods -l application=runner -n mstreamio2g
  kubectl wait pod -l application=runner -n mstreamio2g --for condition=ready --timeout=3600s

  end_time=$(date +%s)
  
  kubectl get pods -l application=runner -n mstreamio2g

  elapsed=$(( end_time - start_time ))

  echo "number of replicas = "$i" time elapsed="$elapsed"s"
  echo $i $elapsed >> output.txt
done
