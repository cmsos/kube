#!/bin/bash

#Extracting pod name
podname=$(kubectl get pods -l application=runner -n gevb -o jsonpath='{.items[0].metadata.name}')

echo "podname = "$podname

datafilepath=$(kubectl exec $podname -n gevb -- find /tmp -type f -maxdepth 1 -name "*.dat")
echo "datafilepath = "$datafilepath

datafile=$(basename -- "$datafilepath")
echo "datafile = "$datafile

outputdir=/usr/repos/data/measurements/daq3val/gevb/20220406/canonicalnuma
mkdir -p $outputdir
echo "outputdir = "$outputdir

kubectl cp gevb/$podname:$datafilepath $outputdir/$datafile
kubectl cp gevb/bu-0:/tmp/gevb.profile $outputdir/$datafile.gevb.profile
kubectl cp gevb/bu-0:/tmp/gevb.configure $outputdir/$datafile.gevb.configure

kubectl get pods -n gevb -o wide > $outputdir/$datafile.info
echo "" >> $outputdir/$datafile.info
kubectl get pod bu-0 -n gevb -o jsonpath={'.status.containerStatuses[0].image'} >> $outputdir/$datafile.info
echo "" >> $outputdir/$datafile.info

ls -l $outputdir
