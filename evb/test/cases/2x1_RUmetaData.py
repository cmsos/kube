import operator

from TestCase import TestCase
from Context import FEROL,RU,BU

class case_2x1_RUmetaData(TestCase):

    def runTest(self):
        self.configureEvB(maxTries=20)
        self.enableEvB()
        self.checkEVM(6144)
        #self.checkRU(8344)
        #self.checkBU(14488)
        # Remove size the FED 1022
        self.checkRU(8192)
        self.checkBU(14336)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU")
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',0,operator.eq,"BU")
        self.stopEvB()
        self.haltEvB()


    def fillConfiguration(self,symbolMap):
        evm = RU(symbolMap,[
             ('inputSource','string','Socket')
            ])
        for id in (512,25,17):
            self._config.add( FEROL(symbolMap,evm,id) )

        ru = RU(symbolMap,[
             ('inputSource','string','Socket')
            ])
        # Removed pseudo FED till the MetaDataRetrieverDIPBridge works
        #for id in range(1,5)+[1022,]:
        for id in range(1,5):            
            self._config.add( FEROL(symbolMap,ru,id) )

        self._config.add( evm )
        self._config.add( ru )

        self._config.add( BU(symbolMap,[
             ('dropEventData','boolean','true'),
             ('lumiSectionTimeout','unsignedInt','0')
            ]) )
