#!/bin/bash -x

export PATH=$PATH:/usr/sbin
kubeadm token create --print-join-command | xargs sudo
