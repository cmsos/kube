#!/bin/bash

HELM_FILE=$1
ZONES=(cdaq3 daq3val minidaq emu muondt ecal es hcal bril pixel rpc tracker trigger tcdsd3v tcdslight tcdsp5 totem ctpps mtd zdc)

for ZONE in "${ZONES[@]}";
do
  helm uninstall $ZONE-coldspot;
done
