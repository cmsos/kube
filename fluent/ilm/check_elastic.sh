#!/bin/bash

while : ; do
    status=$(curl -sS http://elasticsearch-master:9200/_cluster/health?pretty | grep status | sed -r 's/.*"status".*:.*"([a-z]+)".*/\1/g')
    echo "status="$status
    [[ $status != 'green' ]] || break
    sleep 5
done
