// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _play_h_
#define _play_h_

#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>
#include "xdaq/Application.h"
#include "toolbox/TimeVal.h"
#include "nlohmann/json.hpp"

namespace play 
{

class Application: public xdaq::Application
{	
	public:

	static xdaq::Application* instantiate(xdaq::ApplicationStub* s);
    
    	Application(xdaq::ApplicationStub* c): xdaq::Application(c)
	{
		std::thread t([]() {
			unsigned long counter = 0;
			nlohmann::json metrics;
			std::ofstream ofile;
			ofile.open("/metrics/channel", std::ios_base::app);
			while(1) {
        			//std::cout << "play thread " << std::this_thread::get_id()  <<  " with counter " << counter++ << std::endl;

				std::stringstream ss;
				ss << std::this_thread::get_id();

 
				metrics["threadid"] = ss.str();;
				metrics["counter"] = counter++;
				metrics["time"] = toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt);
				ofile << metrics << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(3));
			}
    		});
		t.detach();
  	}

	~Application() {};
    
};
}

#endif
