#!/usr/bin/python3

import time
import getopt, sys
import soap
import measure

#Args used to parameterize 
numreplicas=1
port=0
fragmentsizes = [128,256,512,1000,2000,4000,8000,12000,16000,20000,24000,32000,64000,128000,256000]


full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:p:s:"
long_options = ["help", "replicas=", "port=", "sizes="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: run.py [-r #,--replicas=#] [-p # ,--port=#]  [-s # ,--sizes='s [s ...]']")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-s", "--sizes"):
        #fragmentsizes = current_value.split()
        fragmentsizes = [int(i) for i in current_value.split()]
    else:
        print ("Invalid option")
        sys.exit(2)

#

print("Wait for GEV to be ready...")
expected = numreplicas*3+1
connected = False
while not connected:
    total = 0
    try:
        c = soap.parameter_get("http://evm-0.evm:"+str(port), "gevb2g::EVM", 0, "liveness", "xsd:boolean")
        #print ("Return value from parameter get is " + c)
        if (c == 'true'):
            total += 1
    
        for r in range(numreplicas):
            c = soap.parameter_get("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::RU", r, "liveness", "xsd:boolean")
            #print ("Return value from parameter get is " + c)
            if (c == 'true'):
                total += 1
            c = soap.parameter_get("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r, "liveness", "xsd:boolean")
            #print ("Return value from parameter get is " + c)
            if (c == 'true'):
                total += 1
            c = soap.parameter_get("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::InputEmulator", r, "liveness", "xsd:boolean")
            #print ("Return value from parameter get is " + c)
            if (c == 'true'):
                total += 1
    except Exception as ex:
        print(ex)
    except Fault as ex:
        print(ex)

    if (total == expected):
        connected = True
    print("Found " + str(total) +  " nodes out of " + str(expected))
    time.sleep(5)
print("done readiness.")



try:

    #Run gevb for N replicas
    for fsize in fragmentsizes:
        print("Measure input size " + str(fsize))
        measurements = {}
        measurements['measurement'] = []
        measurements['fragSizeRMS'] = 0
        measurements['fragSize'] = fsize
    
    
        #  setup (input, and bu)
        for r in range(numreplicas):
            test = soap.parameter_get("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::InputEmulator", r, "Mean", "xsd:unsignedLong")
            print (str(test))
            soap.parameter_set("http://ru-" + str(r) + ".ru:" + str(port), "gevb2g::InputEmulator", r, "Mean", "xsd:unsignedLong", fsize)
            soap.parameter_set("http://bu-" + str(r) + ".bu:" + str(port), "gevb2g::BU", r, "currentSize", "xsd:unsignedLong", fsize)
    
    
        #  configure (input, evm, bu, ru )
        soap.configure("http://evm-0.evm:" + str(port), "gevb2g::EVM", 0,)
        for r in range(numreplicas):
            soap.configure("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::InputEmulator", r)
            soap.configure("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::RU", r)
            soap.configure("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r)

        # for r in range(numreplicas):
           # soap.connect("http://ru-"+str(r)+".ru:"+str(port), "pt::ibv::Application", r)

        # print("Wait for pt::ibv to be connected...")
        # expected = numreplicas
        # connected = False
        # while not connected:
            # total = 0
            # try:
                # for r in range(numreplicas):
                    # c = soap.parameter_get("http://ru-"+str(r)+".ru:"+str(port), "pt::ibv::Application", r, "stateName", "xsd:string")
                    # print ("Return value from parameter get (pt::ibv::Application) is " + c)
                    # if (c == 'Enabled'):
                        # total += 1
            # except Exception as ex:
                # print(ex)
            # except Fault as ex:
                # print(ex)

            # if (total == expected):
                # connected = True
            # print("Found " + str(total) +  " nodes out of " + str(expected))
            # time.sleep(5)
        # print("connected.")

        time.sleep(5)

        #  enable (input, evm, bu, ru )
        soap.enable("http://evm-0.evm:"+str(port), "gevb2g::EVM", 0)
        # ru enable
        for r in range(numreplicas):
            soap.enable("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::RU", r)
    
        # bu enable
        for r in range(numreplicas):
            soap.enable("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r)
    
        time.sleep(5)

        # start input
        for r in range(numreplicas):
            soap.enable("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::InputEmulator", r)
    
        time.sleep(30)
        
        # Display eventRate from bus
        for i in range(6):
    
            for r in range(numreplicas):
                rate = soap.parameter_get("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r, "eventRate", "xsd:unsignedInt")
                print ("extracted rate " + rate )
                
            measurements['measurement'].append(measure.retrieve(port,numreplicas))
    
            time.sleep(20)
    
        #add measurement for current size
        f = open("/tmp/gevb-1x"+ str(numreplicas) + ".dat", "a")
        f.write(str(measurements) + "\n")
        f.close()
    
        # print("Exiting before halting input")
        # sys.exit(0)
    
        # halt input
        for r in range(numreplicas):
            soap.halt("http://ru-"+str(r)+".ru:"+str(port) ,"gevb2g::InputEmulator", r)
    
        # halt evm
        soap.halt("http://evm-0.evm:"+str(port), "gevb2g::EVM", 0)
    
        # halt ru
        for r in range(numreplicas):
            soap.halt("http://ru-"+str(r)+".ru:"+str(port), "gevb2g::RU", r)
    
        # halt bu
        for r in range(numreplicas):
            soap.halt("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r)
    
        time.sleep(30)
except Exception as ex:
    print(ex)
except Fault as ex:
        print(ex)


# end of tests

print("done")
