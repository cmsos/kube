#!/bin/bash -x

/usr/sbin/setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

yum install -y yum-utils device-mapper-persistent-data lvm2
yum install -y docker
systemctl enable docker
systemctl start docker

cp ~dsimelev/kubernetes.repo /etc/yum.repos.d/
yum clean all
yum install -y kubelet kubeadm kubectl
systemctl enable kubelet
systemctl start kubelet

touch /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" > /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-iptables = 1" > /etc/sysctl.d/k8s.conf
/usr/sbin/sysctl --system

docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-proxy:v1.16.3
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-apiserver:v1.16.3
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-controller-manager:v1.16.3
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-scheduler:v1.16.3
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/etcd:3.3.15-0
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/coredns:1.6.2
docker pull gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/pause:3.1
docker pull gitlab-registry.cern.ch/cmsos/kube/coreos/flannel:v0.11.0-amd64

docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-proxy:v1.16.3 k8s.gcr.io/kube-proxy:v1.16.3
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-apiserver:v1.16.3 k8s.gcr.io/kube-apiserver:v1.16.3
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-controller-manager:v1.16.3 k8s.gcr.io/kube-controller-manager:v1.16.3
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/kube-scheduler:v1.16.3 k8s.gcr.io/kube-scheduler:v1.16.3
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/etcd:3.3.15-0 k8s.gcr.io/etcd:3.3.15-0
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/coredns:1.6.2 k8s.gcr.io/coredns:1.6.2
docker tag gitlab-registry.cern.ch/cmsos/kube/k8s.gcr.io/pause:3.1 k8s.gcr.io/pause:3.1
docker tag gitlab-registry.cern.ch/cmsos/kube/coreos/flannel:v0.11.0-amd64 quay.io/coreos/flannel:v0.11.0-amd64
