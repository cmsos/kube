Operating Kubernetes


Create a Kubernetes Cluster at CERN

https://clouddocs.web.cern.ch/containers/quickstart.html


`ssh lxplus-cloud.cern.ch`

```
% openstack coe cluster template list
% openstack coe cluster create mykube --keypair cmsos --cluster-template kubernetes-1.15.3-2 --node-count 2
% openstack coe cluster list
% openstack coe cluster config mykube
```
Create a cluster with different flavor

`$ openstack coe cluster create mykube --keypair cmsos --cluster-template kubernetes-1.15.3-2 --node-count 2 --master-flavor m2.large --flavor m2.large`


```
% setenv KUBECONFIG /afs/cern.ch/user/l/lorsini/config

% kubectl get node

% openstack coe cluster resize mykubcluster 4
```


A bare cc7 docker image can be found at https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry 

`gitlab-registry.cern.ch/linuxsupport/cc7-base:latest`

Deploy a CMSOS Application

```
% kubectl create deployment docker-image-cmsos-master-cc7-x64-full  --image=gitlab-registry.cern.ch/cmsos/kube/docker-image-cmsos-master-cc7-x64-full:1.0.0.0

% kubectl get pods
% kubectl describe pod <name>


```

In order delete a deployment

```
% kubectl get all
% kubectl delete deployment.apps/docker-image-cmsos-master
```


Run a POD centos vanilla

`$ kubectl run -it foo --image=centos --restart=Never -- /bin/bash`


Testing k8s files

```
% cd kube/xdaq/cc7-x64/full/k8s

% kubectl create -f ns.yaml
% kubectl get namespaces

% kubectl create -f service.yaml
% kubectl get services -n cmsos

% kubectl create -f deployment.yaml
% kubectl get deployments -n cmsos
% kubectl get svc -n cmsos
% kubectl get pod -n cmsos
%kubectl get pods --output=wide
```

Delete a service

`% kubectl delete services cmsos-xdaq-service -n cmsos`

Remove a deployment

`& kubectl delete deploy -n cmsos cmsos-xdaq-deployement`

Fetch all Pods in all namespaces using 

`% kubectl get pods --all-namespaces`

Log the execution of the application ( use of pod name)

`% kubectl logs -f cmsos-xdaq-deployement-6bc7f4ff6d-8gqbv -n cmsos`

Run a shell inside pod

`% kubectl exec -ti cmsos-xdaq-deployement-84c9f85c76-w6tj9  -n cmsos bash`

```
% kubectl exec -ti cmsos-kubezone-jobcontrol-5b84cd5474-gjjfr -n cmsos -- env COLUMNS="`tput cols`"  LINES="`tput lines`" TERM=$TERM bash
```

Display information about the service

`% kubectl describe services cmsos-xdaq-service -n cmsos`

Use the following URL on browser to access hyperdaq with ( actual NodeIP: NodePort)

`% openstack coe cluster show -c node_addresses mykube `

`http://137.138.32.205:31895/urn:xdaq-application:service=hyperdaq/`



Some info about SOCKS

https://github.com/kubernetes-contrib/jumpserver
https://www.reddit.com/r/kubernetes/comments/avonc2/how_to_connect_to_a_remote_kubernetes_cluster/

https://hub.docker.com/r/serjs/go-socks5-proxy/

https://hub.docker.com/r/vimagick/dante/

https://stackoverflow.com/questions/55431194/how-to-get-kubernetes-pods-to-use-a-transparent-socks5-proxy-for-certain-connect

https://stackoverflow.com/questions/55431194/how-to-get-kubernetes-pods-to-use-a-transparent-socks5-proxy-for-certain-connect


Playing with socks5

```
% kubectl create deployment go-socks5-proxy --image=serjs/go-socks5-proxy:latest
% kubectl get deployments
% kubectl get pods
% kubectl logs go-socks5-proxy-58969f8764-slxrq
% kubectl expose deployment/go-socks5-proxy --type="NodePort" --port 1080
% kubectl get services
% curl --socks5 137.138.32.205:30317 http://ifcfg.co
```

------------------------------------------------

Attaching socks5 proxy pod to a specific node (needed if proxy service does not have External IP/URL)


Listing node names:

```
$ kubectl get nodes
```

Assigning label to a node: kubectl label nodes <node-name> <label-key>=<label-value>
```
$ kubectl label nodes myk8s-36kug4sgcvhh-minion-0 cmsos.role=socks5-proxy
```

Verifying that it succeded
```
$ kubectl get nodes --show-labels myk8s-36kug4sgcvhh-minion-0
```

Starting socks5-proxy deployment and service from files. Attachment of a pod to the specific node is achieved by nodeSelector section in deployment file.
```
$ cd kube/xaas/cc7-x64/full/k8s
$ kubectl create -f proxy-deployment.yaml
$ kubectl get deployment
$ kubectl get pods
$ kubectl logs socks5-proxy-78bc9b599c-l62c2
$ kubectl create -f proxy-service.yaml
$ kubectl get service
```

Checking if pod is running on the right node
```
$ kubectl get pods --output=wide
```

Retrieving internal IP of the node
```
$ kubectl get nodes -o wide
```

Node port is specified in proxy-service.yaml file. It can also be displayed with the following command
```
$ kubectl get services socks5-proxy
NAME           TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
socks5-proxy   NodePort   10.254.2.126   <none>        1080:30000/TCP   5m31s
```
Note: nodePort is 30000 in this example.

Test if proxy is working properly with a command: curl --socks5 [node IP]:[node port] http://ifcfg.co
```
$ curl --socks5 137.138.33.128:30000 http://ifcfg.co
curl: (7) Can't complete SOCKS5 connection to 0.0.0.0:0. (3)
```

New entry should appear in the log of the pod
```
$ kubectl logs socks5-proxy-75c5767466-fxdbn
2019/11/13 11:04:54 Start listening proxy service on port 1080
2019/11/13 12:35:17 [ERR] socks: Failed to handle request: Connect to 2606:4700:30::681c:1ccb:80 failed: dial tcp [2606:4700:30::681c:1ccb]:80: connect: network is unreachable
```
-----------------------------------------------

Try to access Hyperdaq through local IP:  

`% kubectl describe pods -n cmsos`

`% curl --socks5 137.138.32.205:30317 http://10.100.1.110:1972/urn:xdaq-application:service=hyperdaq/processInformation`

also works it has to do with load balancing. A service name is used and forward to the actuall running app

`% http://cmsos-xdaq-service.cmsos:1972/urn:xdaq-application:service=hyperdaq/exploreCluster`

After running host -a 10.100.1.110 the adress resolve to 10-100-1-110.cmsos-xdaq-service.cmsos.svc.cluster.local.

By using hostname and subdomain in the deployement:

`http://myxdaqkube.cmsos-xdaq-service.cmsos:1972/urn:xdaq-application:service=hyperdaq/exploreCluster`

 nslookup does not resolve the pod hostname so the hostname and subdomain must be provided into the deployment to make xdaq using the resolvable hostname.

https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods

To select by label

`% kubectl get pod -n cmsos -l app=xdaq`


The minion nodename is
`% nslookup 137.138.32.205`

mykube-amajdq6zkajs-minion-0.cern.ch


API documentation

[API](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#service-v1-core)


Proxying the Kubernetes cluster

https://www.ovh.com/blog/getting-external-traffic-into-kubernetes-clusterip-nodeport-loadbalancer-and-ingress/

Use NodePort to expose a app to the external world. Need to know minion's names.
Use of default ClusterIP for internal Pod to Pod access.
...

To be read
https://stackoverflow.com/questions/36270602/how-to-access-kubernetes-ui-via-browser


SSHD Jumper

https://www.bonesoul.com/jump-server-kubernetes/

Notice that 

```
set KEY=`cat ~/private/kube.pub |  base64 -w 0`
sed "s/PUBLIC_KEY/$KEY/" secret.yaml.template > secret.yaml
```

According proxy configured on http://xdaq.web.cern.ch/xdaq/cms.pac


`ssh -i ~<username>/private/kube.pem root@mykube-n5l2mzbxm3k3-minion-0.cern.ch -p 31359 -ND 1082`

For usage inside P5 kubernetes kluster

`ssh -t cmsusr -L 1082:localhost:1090 "ssh -ND 1090 -p 31000 -i ~<username>/private/kube root@pc-c2e11-35-01.cms"`


Working with Replicas

To discover all pod for a given services

`% kubectl get endpoints localbus -o json -n cmsos`

Access from within a POD using curl

https://stackoverflow.com/questions/30690186/how-do-i-access-the-kubernetes-api-from-within-a-pod-container

https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/

Log into the POD ( using bash ) and run the following:

```
# Point to the internal API server hostname
APISERVER=https://kubernetes.default.svc

# Path to ServiceAccount token
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount

# Read this Pod's namespace
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)

# Read the ServiceAccount bearer token
TOKEN=$(cat ${SERVICEACCOUNT}/token)

# Reference the internal certificate authority (CA)
CACERT=${SERVICEACCOUNT}/ca.crt

# Explore the API with TOKEN
curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api

```


Explain how to use roles and binding and service accounts
Example from https://ansilh.com/14-accounts_and_rbacs/04-service-accounts/

Use 

```
$ kubectl create -f cmsos-accounts.yaml
$ kubectl exec -it mydebugger -n cmsos -- /bin/sh
```


inside run 

```
APISERVER=https://${KUBERNETES_SERVICE_HOST}:${KUBERNETES_SERVICE_PORT_HTTPS}
TOKEN=$(cat /run/secrets/kubernetes.io/serviceaccount/token)
curl $APISERVER/api/v1/pods  --header "Authorization: Bearer $TOKEN" --cacert /run/secrets/kubernetes.io/serviceaccount/ca.crt
```


**Combining Docker, Kubernetes and CI**

https://about.gitlab.com/blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/

https://gitlab.com/gitlab-examples/kubernetes-deploy



Using configmap for xdaq configuration

https://unofficial-kubernetes.readthedocs.io/en/latest/tasks/configure-pod-container/configmap/

https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/

**Create the map with profiles**

`% kubectl create configmap myxaas --from-file=opt/xdaq/share/kubezone/profile`

Attach to pod

`% kubectl create -f podwithconfig.map`

Also uses kustomization

`cd kube/xaas/cc7-x64/full`

kustomization.yaml

`$ kubectl apply -k .`

kubectl apply -k .
