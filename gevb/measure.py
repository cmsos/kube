import soap


def retrieve(port,replicas):
    sample = {}
    m  = {}
    fragmentRate = 0
    for r in range(replicas):
        m['BU'+ str(r)] = int(soap.parameter_get("http://bu-"+str(r)+".bu:"+str(port), "gevb2g::BU", r, "eventRate", "xsd:unsignedInt"))
        fragmentRate += int(m['BU'+ str(r)])
    for r in range(replicas):
        m['RU'+ str(r)] = fragmentRate
    m['EVM0'] = fragmentRate 

    sample['rates'] = m;

    s  = {}
    s['EVM0'] = 0
    eventSize = 0
    for r in range(replicas):
        s['RU'+ str(r)] = int(soap.parameter_get("http://ru-" + str(r)+ ".ru:" + str(port), "gevb2g::InputEmulator", r, "Mean", "xsd:unsignedLong"))
        eventSize +=  int(s['RU'+ str(r)])
    for r in range(replicas):
        s['BU'+ str(r)] = eventSize
    sample['sizes'] = s;

    print ("retrieved sample " + str(sample))
    return sample

