#!/bin/bash -x

kubectl delete node `hostname`
sudo kubeadm reset --force

sudo systemctl stop kubelet
sudo systemctl stop docker

sudo systemctl disable kubelet
sudo systemctl disable docker

sudo yum remove -y docker
sudo yum remove -y kubelet kubeadm kubectl
