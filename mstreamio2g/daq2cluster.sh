function wassh_clients { mussh ${mussh_params} -h `hostlist 'ru-c2e(12|14)' 'ru-c2e12-40-01'` -c "$@"; }
export -f wassh_clients

function wassh_servers { mussh ${mussh_params} -h `hostlist 'ru-c2e(13|15)' 'ru-c2e13-37-01'` -c "$@"; }
export -f wassh_servers

function wassh_hosts { mussh ${mussh_params} -h `hostlist 'ru-c2e(12|13|14|15)' 'ru-c2e12-40-01|ru-c2e13-37-01'` -c "$@"; }
export -f wassh_hosts

function wassh_master { mussh ${mussh_params} -h `hostlist 'ru-c2e12-40-01'` -c "$@"; }
export -f wassh_master
