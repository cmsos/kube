import operator

from TestCase import TestCase
from Context import RU,BU

class case_3x1_metaData(TestCase):

    def runTest(self):
        self.configureEvB(maxTries=20)
        self.enableEvB()
        self.checkEVM(6144)
        self.checkRU(24576,1)
        #self.checkRU(152,2)
        #self.checkBU(30872)
        # Remove size the FED 1022
        self.checkRU(2048,2)
        self.checkBU(32768)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU")
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',0,operator.eq,"BU")
        self.stopEvB()
        self.haltEvB()


    def fillConfiguration(self,symbolMap):
        self._config.add( RU(symbolMap,[
             ('inputSource','string','Local'),
             ('fedSourceIds','unsignedInt',(512,25,17))
            ]) )
        self._config.add( RU(symbolMap,[
             ('inputSource','string','Local'),
             ('fedSourceIds','unsignedInt',list(range(1,13)))
            ]) )
        self._config.add( RU(symbolMap,[
             ('inputSource','string','Local'),
             # Removed pseudo FED till the MetaDataRetrieverDIPBridge works
             #('fedSourceIds','unsignedInt',(1022,))
             ('fedSourceIds','unsignedInt',(15,))
            ]) )
        self._config.add( BU(symbolMap,[
             ('dropEventData','boolean','true'),
             ('lumiSectionTimeout','unsignedInt','0')
            ]) )
