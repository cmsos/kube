oracledb = require('oracledb');
const { v4: uuidv4 } = require('uuid');
oracledb.autoCommit = true;

const data = {
	"table": {
		"definition": [{
				"key": "uniqueid",
				"type": "string"
			},
			{
				"kez": "storeTime",
				"type": "time"
			},
			{
				"key": "dateTime",
				"type": "time"
			},
			{
				"key": "identifier",
				"type": "string"
			},
			{
				"key": "severity",
				"type": "string"
			},
			{
				"key": "occurrences",
				"type": "string"
			},
			{
				"key": "notifier",
				"type": "string"
			},
			{
				"key": "context",
				"type": "string"
			},
			{
				"key": "message",
				"type": "string"
			},
			{
				"key": "class",
				"type": "string"
			},
			{
				"key": "args",
				"type": "string"
			}
		],
		"rows": []
	}
}

class DataBase {
	constructor(connection) {
		this.connection = connection;
	}

	async lastStoredEvents(start) {
		try {
			//console.log("db lastStoredEvents start time " +start);
			var since = start.getTime() / 1000;
			//console.log("since in float is " +since);
			var response = await this.connection.execute(`select * from (select event.*,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version,class,instance,
lid,context,groups,service,zone,uuid,args from event left join catalog on (event.exception=catalog.uniqueid) left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid) where storeTime> :since ORDER BY storeTime) where rownum <= 10`, [since], { outFormat: oracledb.OBJECT });

			//console.log("Metadata: ");
			//console.dir(response.metaData, { depth: null });
			//console.log("Query results: ");
			//console.dir(response.rows);
			var maxStoreTime = 0.0;
			var events = [];
			for (const element of response.rows) {
				var storeTime = element["STORETIME"] + 0.5; // add 0.5 prevent step back in time approssimation
				if ( storeTime > maxStoreTime ) {
					maxStoreTime = storeTime;
				}
				var event = {};
				for (const [key, value] of Object.entries(element)) {
					if ( key == "DATETIME" ) {  
						event["dateTime"] = value;
					} else if ( key == "STORETIME" ) { 
						event["storeTime"] = value;
					} else if ( key == "FUNCTION" ) { 
						event["func"] = value;
					} else if ( key == "TYPE" ) { 
						if ( value == "clear" ) {
							event["type"] = "rearm";
						} else {
							event["type"] = value;
						}

					} else { 
						event[key.toLowerCase()] = value;
					}
				}
				events.push(event);
			}

			//const jsonStr = JSON.stringify(result.rows);
			//console.log("json is " + jsonStr);
			data.table.rows = events;
			var event = new Date(maxStoreTime * 1000);
			data.lastStoreTime = event.toISOString();

			//console.log("table is " + JSON.stringify(data));
			//console.log("db.maxStoreTime is " + maxStoreTime + " ISO string is " + data.lastStoreTime);
			return data; // result in required json 

		} catch (err) {
			console.error(err);
			//if connection to database is lost
			if (err.errorNum == 1012) {
				console.log("Connection to Oracle database is lost. Exiting.")
				process.exit(1);
			}
		}
	}

	async ping() {
		try {
			const response = await this.connection.execute('select count(*) as "count" from event', {}, { outFormat: oracledb.OBJECT, fetchArraySize: 1 });
			var count = response.rows[0]["count"];
			return {"count": count};
		} catch (err) {
			console.error(err);
			process.exit(1);
		}
	}

	async query (statement) {
		try {
			const response = await this.connection.execute(statement, {}, { outFormat: oracledb.OBJECT });
			var events = [];
			for (const element of response.rows) {
				var event = {};
				for (const [key, value] of Object.entries(element)) {
					if ( key == "DATETIME" ) {
						event["dateTime"] = value;
					} else if ( key == "STORETIME" ) {
						event["storeTime"] = value;
					} else if ( key == "FUNCTION" ) {
						event["func"] = value;
					} else {
						var escapedValue;
						if (!value) {
							escapedValue = "";
						} else {
							escapedValue = JSON.parse(JSON.stringify(value));
						}
						event[key.toLowerCase()] = escapedValue;
					}
				}
				events.push(event);
			}
			data.table.rows = events;
			return data;
		} catch (err) {
			console.error(err);
			//if connection to database is lost
			if (err.errorNum == 1012) {
				console.log("Connection to Oracle database is lost. Exiting.")
				process.exit(1);
			}
		}
	}

	async view (uniqueid) {
		oracledb.fetchAsBuffer = [ oracledb.BLOB ];
		try {
			const response = await this.connection.execute('select exception from exceptions where uniqueid = :uniqueid', [uniqueid]);

			if (response.rows.length === 0) {
				return JSON.parse("{}")
			} else {
				const blob = response.rows[0][0];
				try {
					const e = JSON.parse(blob.toString())
					return e;
				} catch(err) {
					console.error("Failed BLOB parsing " + err);
					return JSON.parse("{}");
				}
			}
		} catch (err) {
			console.error("Failed BLOB retrieval " + err);
			//if connection to database is lost
			if (err.errorNum == 1012) {
				console.log("Connection to Oracle database is lost. Exiting.")
				process.exit(1);
			}
			return JSON.parse("{}");
		}
	}

	async rearm (exceptions,source) {
		//console.log("d.uids  is "+exceptions);
		var currenttime =  new Date();
		var storeTime = currenttime.getTime() / 1000;
		var creationTime = storeTime;
		var type = "clear";

		for(let exception of exceptions) {

			let eid = uuidv4();

			const statement = "INSERT INTO event VALUES ('" + eid + "', " + storeTime + ", '" + exception + "', '" + source + "', '" + type + "', " + creationTime + ")";

			//console.log("d.exceptin is "+exception);
			//console.log("d.eid is "+eid);
			//console.log("d.storeTime is "+storeTime);
			//console.log("d.source is "+source);
			//console.log("d.creationTime is "+creationTime);
			//console.log("d.statement is "+statement);

			try {
				const response = await this.connection.execute(statement);
			} catch (err) {
				console.error("Failed to rearm" + err);
				//if connection to database is lost
				if (err.errorNum == 1012) {
					console.log("Connection to Oracle database is lost. Exiting.")
					process.exit(1);
				}
				return;
			}
		}
	}
}

module.exports = class DataBaseFactory {
	async create(user,pass,args) {
		try {
			return new DataBase(await oracledb.getConnection({ user : user, password : pass, connectString : args }));
		} catch (err) {
			console.error(err);
			process.exit(1);
		}
	}
}
