#!/bin/bash

export KUBECONFIG=~/.kube/config

export KEY=`cat ~/private/kube.pub | base64 -w 0`
sed "s/PUBLIC_KEY/$KEY/" secret.yaml.template > secret.yaml
kubectl create -f secret.yaml
rm secret.yaml

kubectl create -f sshd-jumpserver.yaml
