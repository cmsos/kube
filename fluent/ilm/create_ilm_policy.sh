#!/bin/bash

set -x

curl -sS -H "Content-Type: application/json" -XPUT "http://elasticsearch-master:9200/_ilm/policy/fluent-bit?pretty" -d @policy.json -o /tmp/output1.txt
cat /tmp/output1.txt
curl -sS -H "Content-Type: application/json" -XPUT "http://elasticsearch-master:9200/_template/fluent-bit-template?pretty" -d @index_template.json -o /tmp/output2.txt
cat /tmp/output2.txt
