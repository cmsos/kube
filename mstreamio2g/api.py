#!/usr/bin/python3

import ssl
ssl._create_default_https_context = ssl._create_unverified_context

import time
import getopt, sys
import http.client
#Args used to parameterize 
host=''
port=0
token=''
path=''

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "h:u:p:e:t:"
long_options = ["help", "host=", "port=", "path=", "token="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: api.py -u,--host=<hostname> -p,--port=# -e,--path=<path>  -t,--token=<token>")
        sys.exit(0)
    elif current_argument in ("-u", "--host"):
        host = current_value
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-e", "--path"):
        path = current_value
    elif current_argument in ("-t", "--token"):
        token = current_value
    else:
        print ("Invalid option")
        sys.exit(2)

#print(host)
#print(port)
#print(path)
#print(token)

headers={'Authorization': 'Bearer ' + token }
conn = http.client.HTTPSConnection(host,port)
conn.request("GET", path, "", headers)
response = conn.getresponse()
replymessage = str(response.read().decode());
print(replymessage)
