#!/usr/bin/python3

from string import Template
import getopt, sys

#Args used to parameterize 
numreplicas=1
interface='ens4f0'
gidtype='v2'
port=0
endpointhost='endpoint'
endpointport=0
endpointdomain='roce'

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:i:t:p:e:m:f:"
long_options = ["help", "replicas=", "interface=", "gidtype=", "port=", "endpointhost=", "endpointport=", "endpointdomain="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: autogen.py [-r #,--replicas=#] [-i name,--interface=name] [-t type,--gidtype=type] [-p # ,--port=#]  [-e name,--endpointhost=name] [-m name,--endpointdomain=name] [-f # ,--endpointport=#]")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-i", "--interface"):
        interface = current_value
    elif current_argument in ("-t", "--gidtype"):
        gidtype = current_value
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-e", "--endpointhost"):
        server = current_value
    elif current_argument in ("-m", "--endpointdomain"):
        domain = current_value
    elif current_argument in ("-f", "--endpointport"):
        endpointport = int(current_value)


#Set dictionary for template expansion
dict = {'endpointport':endpointport, 'interface':interface, 'gidtype':gidtype, 'port':port, 'replica':'0', 'unicast':''}
#Templates XML 

unicastt=Template("""<xc:Unicast class="$classname" instance="$replica" network="$network"/>""")

nodet = Template("""
    <xc:Context url="http://node-$replica.node:$port">
            
        <pol:policy xmlns:pol="http://xdaq.web.cern.ch/xdaq/xsd/2013/XDAQPolicy-10">
            <pol:element pattern="urn:shub-thread:control" type="thread" package="numa" mempolicy="onnode" memnode="1"  affinity="10" />
            <pol:element pattern="urn:shub-thread:input" type="thread" package="numa"  mempolicy="onnode" memnode="1"  affinity="11" />
            <pol:element pattern="urn:shub-thread:shuffle" type="thread" package="numa"  mempolicy="onnode" memnode="1"  affinity="12" />
            <pol:element pattern="urn:shub-thread:build" type="thread" package="numa"  mempolicy="onnode" memnode="1"  affinity="13" />
        </pol:policy>
            
        <xc:Endpoint protocol="vpi" service="pipe" hostname="node-$replica.node" port="${endpointport}0" network="arbiterctrlnet" />
        <xc:Endpoint protocol="vpi" service="pipe" hostname="node-$replica.node" port="${endpointport}1" network="nodercinputnet" />
        <xc:Endpoint protocol="vpi" service="pipe" hostname="node-$replica.node" port="${endpointport}2" network="nodercdatanet"/>
        <xc:Endpoint protocol="vpi" service="pipe" hostname="node-$replica.node" port="${endpointport}3" network="nodercctrlnet"/>
        <xc:Endpoint protocol="vpi" service="pipe" hostname="node-$replica.node" port="${endpointport}4" network="arbiterinputnet"/>
        
        <xc:Application class="pt::vpi::Application" id="11" instance="0" network="local">
            <properties xmlns="urn:xdaq-application:pt::vpi::Application" xsi:type="soapenc:Struct">
                <!--networkInterface xsi:type="xsd:string">$interface</networkInterface-->
                <iaName xsi:type="xsd:string">$interface</iaName>
                <GIDType xsi:type="xsd:string">$gidtype</GIDType>
                <portNumber xsi:type="xsd:unsignedInt">1</portNumber>
                <poolSize xsi:type="xsd:unsignedLong">0x7f8a00000</poolSize>
                <completionQueueSize xsi:type="xsd:unsignedInt">1024</completionQueueSize>
                <sendQueuePairSize xsi:type="xsd:unsignedInt">1024</sendQueuePairSize>
                <recvQueuePairSize xsi:type="xsd:unsignedInt">1024</recvQueuePairSize>
                <deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
                <allowLocalPipeSupport xsi:type="xsd:boolean">true</allowLocalPipeSupport>
            </properties>
        </xc:Application>
        
        <xc:Application class="shub::InputRC" id="20" instance="$replica" network="local">
            <xc:Unicast class="shub::NodeRC" instance="$replica" network="nodercinputnet"/>
            <xc:Unicast class="shub::Arbiter" instance="$replica" network="arbiterinputnet"/>
            <properties xmlns="urn:xdaq-application:shub::NodeRC" xsi:type="soapenc:Struct">
                <maxBlockSize xsi:type="xsd:unsignedInt">262144</maxBlockSize>
                <poolName xsi:type="xsd:string">vpi</poolName>
                <superFragmentSize xsi:type="xsd:unsignedInt">32768</superFragmentSize>
            </properties>
        </xc:Application>
        
        <xc:Application class="shub::Arbiter" id="30" instance="$replica" network="arbiterctrlnet">
            ${unicast}
            <properties xmlns="urn:xdaq-application:shub::Arbiter" xsi:type="soapenc:Struct">
                <maxResources xsi:type="xsd:unsignedInt">4</maxResources>
                <maxBlockSize xsi:type="xsd:unsignedInt">8192</maxBlockSize>
                <maxGather xsi:type="xsd:unsignedInt">4</maxGather>
                <poolName xsi:type="xsd:string">vpi</poolName>
            </properties>
        </xc:Application>
        
        <xc:Application class="shub::NodeRC" id="40" instance="$replica" network="nodercdatanet">
            <xc:Unicast class="shub::Arbiter" instance="$replica" network="arbiterctrlnet"/>
            <xc:Unicast class="shub::NodeRC" instance="$replica" network="nodercdatanet"/>
            <properties xmlns="urn:xdaq-application:shub::NodeRC" xsi:type="soapenc:Struct">
                <maxResources xsi:type="xsd:unsignedInt">4</maxResources>
                <maxBlockSize xsi:type="xsd:unsignedInt">262144</maxBlockSize>
                <poolName xsi:type="xsd:string">vpi</poolName>
                <maxGather xsi:type="xsd:unsignedInt">4</maxGather>
                <!-- superFragmentSize xsi:type="xsd:unsignedInt">131072</superFragmentSize -->
                <superFragmentSize xsi:type="xsd:unsignedInt">32768</superFragmentSize>
            </properties>
        </xc:Application>
        
        <xc:Application class="xmem::probe::Application" id="21" instance="0" network="local" />
        
        <xc:Module>$${XDAQ_ROOT}/lib/libptvpi.so</xc:Module>
        <xc:Module>$${XDAQ_ROOT}/lib/libshub.so</xc:Module>
        <xc:Module>$${XDAQ_ROOT}/lib/libxmemprobe.so</xc:Module>
        
    </xc:Context>""")
    

#Generate configuration XML 


print("""<xc:Partition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30">""")

# Create unicast block for arbiter
unicast=""
for x in range(numreplicas):
    u = unicastt.substitute(replica=x, network='nodercctrlnet', classname='shub::NodeRC')
    unicast += u + "\n"

for x in range(numreplicas):
    dict['replica'] = x
    dict['unicast'] = unicast
    r = nodet.substitute(dict)
    print (r)
print ("""</xc:Partition>""")
