#!/bin/bash

#Extracting pod name
podname=$(kubectl get pods -l application=runner -n shubml -o jsonpath='{.items[0].metadata.name}')

echo "podname = "$podname

datafilepath=$(kubectl exec $podname -n shubml -- find /tmp -type f -maxdepth 1 -name "*.dat")
echo "datafilepath = "$datafilepath

datafile=$(basename -- "$datafilepath")
echo "datafile = "$datafile

outputdir=/usr/repos/data/measurements/daq3val/shubml/20220331/maxgather
mkdir -p $outputdir
echo "outputdir = "$outputdir

kubectl cp shubml/$podname:$datafilepath $outputdir/$datafile
kubectl exec node-0 -n shubml -- cp /opt/xdaq/share/shubml/profile/profile.yaml /tmp
kubectl cp shubml/node-0:/tmp/profile.yaml $outputdir/$datafile.profile.yaml
kubectl exec node-0 -n shubml -- cp /opt/xdaq/share/shubml/configure/configuration.yaml /tmp
kubectl cp shubml/node-0:/tmp/configuration.yaml $outputdir/$datafile.configuration.yaml

kubectl get pods -n shubml -o wide > $outputdir/$datafile.info
echo "" >> $outputdir/$datafile.info
kubectl get pod node-0 -n shubml -o jsonpath={'.status.containerStatuses[0].image'} >> $outputdir/$datafile.info
echo "" >> $outputdir/$datafile.info

ls -l $outputdir
