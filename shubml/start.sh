#!/bin/bash

helm install --set zone.name=shubml,zone.application.replicas=2 cmsos-kube-helm-shubml chart/cmsos-kube-helm-shubml-1.0.0.tgz

runnercount=0
until [ "$runnercount" -eq "1" ];
do
  runnercount=$(kubectl get deployment runner -n shubml -o=jsonpath='{.status.readyReplicas}')
  if [ -z "$runnercount" ]
  then
    runnercount=0
  fi
done;

kubectl get pods -n shubml
