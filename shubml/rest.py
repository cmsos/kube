#from urllib.request import urlopen
#from urllib.request import Request
import urllib.request

def get(url):
    try:
        response = urllib.request.urlopen(url)
        str = response.read().decode('utf-8')
        return str
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed to get from " + url + " with error " + str(ex))
    except Fault as ex:
        raise ex

def put(url, data):
    print("data = " + data)
    value = data.encode('ascii')
    req = urllib.request.Request(url=url, data=value, method='PUT')
    req.add_header('Content-Type', 'application/json')
    try:
        response = urllib.request.urlopen(req)
        #print(response.status)
        #print(response.reason)
        return response.status
    except Exception as ex:
        print("Error: %s" % ex)
        raise Exception("Failed to put to " + url + " with error " + str(ex))
    except Fault as ex:
        raise ex
