const commandArgs = process.argv.slice(2);
console.log('commandArgs: ', commandArgs);
const oclfile = commandArgs[0];
const zonename = commandArgs[1]
const port = commandArgs[2];

const express = require('express');
Spotlight = require('./spotlight.js');

spotlight = new Spotlight();

app = spotlight.create(oclfile, zonename);

app.use('/', express.static('react/build'));

app.listen(port, () => {
	console.log(`Listening on http://localhost:${port}`);
})
