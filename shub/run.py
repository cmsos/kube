#!/usr/bin/python3

import time
import getopt, sys
import soap
import measure

#Args used to parameterize 
numreplicas=1
port=0
fragmentsizes = [128,256,512,1000,2000,4000,8000,12000,16000,20000,24000,32000,64000,128000,256000]

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:p:s:"
long_options = ["help", "replicas=", "port=", "sizes="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: run.py [-r #,--replicas=#] [-p # ,--port=#]  [-s # ,--sizes='s [s ...]'] ")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-s", "--sizes"):
        #fragmentsizes = current_value.split()
        fragmentsizes = [int(i) for i in current_value.split()]
    else:
        print ("Invalid option")
        sys.exit(2)

#Run shub for N replicas

print("Wait for nodes to be connected...", flush=True)
connected = False
while not connected:
    total = 0
    for r in range(numreplicas):
        try:
            c = soap.parameter_get("http://node-"+str(r)+".node:"+str(port), "shub::NodeRC", r, "connectedness", "xsd:boolean")
            #print ("Return value from parameter get is " + c, flush=True)
            if (c == 'true'):
                total += 1
        except Exception as ex:
            print(ex)
        except Fault as ex:
            print(ex)

    if (total == numreplicas):
        connected = True
    else:
        time.sleep(1)

    print("Found " + str(total) +  " nodes", flush=True)
print("done connectedness.", flush=True)

f = open("/tmp/runner-alive", "x")
f.close()

sys.exit(0)

time.sleep(10)
try:

    for fsize in fragmentsizes:
        print("Measure input size " + str(fsize))
        measurements = {}
        measurements['measurement'] = []
        measurements['fragSizeRMS'] = 0
        measurements['fragSize'] = fsize

        #  setup (input, and bu)
        for r in range(numreplicas):
            soap.parameter_set("http://node-" + str(r) + ".node:" + str(port), "shub::InputRC", r, "superFragmentSize", "xsd:unsignedInt", fsize)
            soap.parameter_set("http://node-" + str(r) + ".node:" + str(port), "shub::NodeRC", r, "superFragmentSize","xsd:unsignedInt", fsize)

        time.sleep(30)
        
        # Display eventRate from bus
        for i in range(6):

            for r in range(numreplicas):
                rate = soap.parameter_get("http://node-"+str(r)+".node:"+str(port), "shub::NodeRC", r, "eventRate", "xsd:unsignedInt")
                print ("extracted rate " + rate )
        
            measurements['measurement'].append(measure.retrieve(port,numreplicas))
            print (measurements)
            
            time.sleep(10)


        f = open("/tmp/shub-"+ str(numreplicas) + "-k8s-native.dat", "a")
        f.write(str(measurements) + "\n")
        f.close()

    # end of tests

    print("done.")
except Exception as ex:
    print(ex)
except Fault as ex:
        print(ex)

