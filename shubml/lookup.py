#!/usr/bin/python3

import getopt, sys, json, time
from k8s import *

#Args used to parameterize
servicename=""
numreplicas=1
k8shost=""
k8sport=0
k8stoken=""
k8snamespace=""

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:s:k:p:t:n:"
long_options = ["help", "servicename=", "replicas=", "k8shost=", "k8sport=", "k8stoken=", "k8snamespace="]

try:
	arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
	# Output error, and return with an error code
	print (str(err))
	sys.exit(2)

for current_argument, current_value in arguments:
	if current_argument in ("-h", "--help"):
		print ("Usage: lookup.py [-r #,--replicas=#] [-s name,--servicename=name] [-k host,--k8shost=host] [-p #,--k8sport=#] [-t token,--k8stoken=token] [-n namespace,--k8snamespace=namespace]")
		sys.exit(0)
	elif current_argument in ("-r", "--replicas"):
		numreplicas = int(current_value)
	elif current_argument in ("-s", "--servicename"):
		servicename = current_value
	elif current_argument in ("-k", "--k8shost"):
		k8shost = current_value
	elif current_argument in ("-p", "--k8sport"):
		k8sport = int(current_value)
	elif current_argument in ("-t", "--k8stoken"):
		k8stoken = current_value
	elif current_argument in ("-n", "--k8snamespace"):
		k8snamespace = current_value

#Check if all the pods were discovered by service
#loop until they are all available
path = '/api/v1/namespaces/' + k8snamespace + '/endpoints/' + servicename
while True:
	response = api(k8shost, k8sport, path, k8stoken)
	document = json.loads(response)
	endpointcount = 0
	if "subsets" in document:
		for subset in document["subsets"]:
			#print("subset = " + str(subset))
			endpointcount += len(subset["addresses"])
		if endpointcount != numreplicas:
			time.sleep(1)
			print("endpointcount = " + str(endpointcount) + " numreplicas = " + str(numreplicas))
		else:
			break
	else:
		time.sleep(1)
print("endpointcount = " + str(endpointcount))

#Retrieve endpoinds and store information into /etc/hosts
hostsfile = open('/etc/hosts', 'a')
for subset in document["subsets"]:
	for address in subset["addresses"]:
		print("address = " + str(address))
		print(address["ip"] + " " + address["hostname"] + "." + servicename, file=hostsfile)
hostsfile.close()
