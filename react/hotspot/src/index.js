import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { Route } from 'react-router';
import { BrowserRouter, withRouter } from 'react-router-dom';
import queryString from 'query-string';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';

import LinearProgress from '@material-ui/core/LinearProgress';

import { orange } from '@material-ui/core/colors';
import LensIcon from '@material-ui/icons/Lens';
import PanoramaFishEyeIcon from '@material-ui/icons/PanoramaFishEye';
import WhatshotIcon from '@material-ui/icons/Whatshot';

class JSONView extends React.Component {
  render() {
    return (
      <div>
        <pre>
          <code>
            {JSON.stringify(this.props.exception, null, 2)}
          </code>
        </pre>
      </div>
    );
  }
}

class HTMLView extends React.Component {
  generateHTML(input) {
    var originatedby = "";
    if ("children" in input) {
      originatedby = "originated by " + this.generateHTML(input.children[0]);
    }

    var output = "<b>" + input.label + "</b>\n";
    output += "<br>\n  <table>\n";

    for (const property of input.properties) {
      output += "    <tr>\n";
      output += "      <td>\n";
      output += "        " + property.name + "\n";
      output += "      </td>\n";
      var value = property.value;
      if (property.name === "message") {
        value = decodeURI(value);
      }
      output += "      <td>\n";
      output += "        " + value + "\n";
      output += "      </td>\n";
      output += "    </tr>\n";
    }

    output += "  </table>\n<br>\n";
    output += originatedby;
    return output;
  }

  render () {
    return (
      <div>
        <pre>
          <code>
            {this.generateHTML(this.props.exception)}
          </code>
        </pre>
      </div>
    );
  }
}

class HierarchicalView extends React.Component {
  styles

  constructor(props) {
    super(props);

    this.styles = makeStyles({
      root: {
        height: 216,
        flexGrow: 1,
        maxWidth: 400,
      },
      table: {
        minWidth: 650,
      },
    });

    this.state = {
      expanded: [],
      selected: []
    }   
  }

  generateTreeItems(exception) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }
    if ("children" in exception) {
      return (
        <TreeItem nodeId={uniqueid} label={exception.label}>{this.generateTreeItems(exception.children[0])}</TreeItem>
      );  
    } else {
      return (
        <TreeItem nodeId={uniqueid} label={exception.label} />
      );  
    }
  }

  retrieveSelectedException(exception, selectedId) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }

    if (uniqueid === selectedId) {
      return exception.properties;
    } else if ("children" in exception) {
      return this.retrieveSelectedException(exception.children[0], selectedId);
    } else {
      return [];
    }
  }

  getRootExceptionId(exception) {
    var uniqueid = "";
    for (const property of exception.properties) {
      if (property.name === "uniqueid") {
        uniqueid = property.value;
        break;
      }
    }
    return uniqueid;
  }

  componentDidMount() {
    if (this.props.exception) {
      var rootExceptionId = this.getRootExceptionId(this.props.exception);
      this.setState({
        expanded: [rootExceptionId],
        selected: [rootExceptionId],
      });      
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.exception !== prevProps.exception) {
      var rootExceptionId = this.getRootExceptionId(this.props.exception);
      this.setState({
        expanded: [rootExceptionId],
        selected: [rootExceptionId],
      });      
    }
  }

  render() {
    const classes = this.styles;

    const handleToggle = (event, nodeIds) => {
      this.setState({
        expanded: nodeIds,
      });
    };
  
    const handleSelect = (event, nodeId) => {
      this.setState({
        selected: [nodeId],
      });
    };
  
    var selectedExceptionProps = [];
    if (this.state.selected.length !== 0) {
      selectedExceptionProps = this.retrieveSelectedException(this.props.exception, this.state.selected[0]);
    };

    return (
      <div id="hierarchy-view-row">
        <div id="hierarchy-view-tree">
          <TreeView
            className={classes.root}
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            expanded={this.state.expanded}
            selected={this.state.selected}
            onNodeToggle={handleToggle}
            onNodeSelect={handleSelect}
          >
            {this.generateTreeItems(this.props.exception)}
          </TreeView>
        </div>
        <div id="hierarchy-view-table">
          <Paper variant="outlined" elevation={0}>
            <TableContainer>
              <Table className={classes.table} size="small" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Value</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {selectedExceptionProps.map((row) => (
                    <TableRow key={row.name}>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell>
                        {(row.name === "message") ? decodeURI(row.value) : row.value}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </div>
      </div>
    );
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class ExceptionViewer extends React.Component {
  exception
  styles

  constructor(props) {
    super(props);
    this.exception = null;
    this.state = {
      error: null,
      isLoaded: false,
      exceptionid: this.props.exceptionid,
      value: 0
    }

    this.styles = makeStyles((theme) => ({
      root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
      },
    }));    
  }

  fetchException() {
    // Fetch exception for display in viewer
    fetch(this.props.spotlighturl + "view?uniqueid=" + this.props.exceptionid)
      .then(res => res.json())
      .then(
        (result) => {
          this.exception = result;
          this.setState({
            error: null,
            isLoaded: true,
            exceptionid: this.props.exceptionid
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.exception = null;
          this.setState({
            error: error,
            isLoaded: true,
            exception: null
          });
        }
      )
  }

  componentDidMount() {
    if (this.props.exceptionid) {
      this.fetchException();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.exceptionid !== prevProps.exceptionid) {
      this.fetchException();
    }
  }

  render() {
    const { error, isLoaded, exceptionid, value } = this.state;
    const classes = this.styles;

    const handleChange = (event, newValue) => {
      this.setState({
        value: newValue
      });
    };

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded && exceptionid) {
      return <div>Loading...</div>;
    } else if (exceptionid){
      return (
        <div className={classes.root}>
          <AppBar position="static">
            <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
              <Tab label="JSON" {...a11yProps(0)} />
              <Tab label="HTML" {...a11yProps(1)} />
              <Tab label="Hierarchy" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <JSONView exception={this.exception} />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <HTMLView exception={this.exception} />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <HierarchicalView exception={this.exception} />
          </TabPanel>
        </div>
      );
    } else {
      return ( <div></div> );
    }
  }
}

function SeverityIcon(props) {
  if (props.shape === "filled") {
    return (
      <LensIcon style={{ fontSize: 10, color: props.color }}/>
    );
  } else {
    return (
      <PanoramaFishEyeIcon style={{ fontSize: 10, color: props.color }}/>
    );
  }
}

function CellContent(props) {
  var textStyle = null;
  var iconShape = null;
  if (props.rowType === "fire") {
    textStyle = { fontWeight: 'bold' };
    iconShape = "filled";
  } else {
    textStyle = { fontWeight: 'normal' };
    iconShape = "outlined";
  }

  if ( props.columnType === "icon" ) {
    var iconColor = null;
    switch(props.value) {
      case "fatal":
        iconColor = 'red';
        break;
      case "error":
        iconColor = 'orange';
        break;
      case "warning":
        iconColor = 'yellow';
        break;
      default:
        iconColor = 'grey';
    }

    return (
      <div class="centered-label">
        <SeverityIcon color={iconColor} shape={iconShape}/>
      </div>
    );
  } else {
    return (
      <div class="centered-label">
        <span style={textStyle}>{props.value}</span>
      </div>
    );
  }
}

class ExceptionList extends React.Component {
  columns
  rows
  styles
  refreshInterval
  
  constructor(props) {
    super(props);

    // Initially fetch all exceptions which are mostly 10 minutes old
    const timeWindow = 10;
    var timeWindowStart = new Date();
    timeWindowStart.setMinutes(timeWindowStart.getMinutes() - timeWindow);

    this.state = {
      error: null,
      isLoaded: false,
      exceptionList: [],
      page: 0,
      rowsperpage: 10,
      selectedid: null,
      lastStoreTime: timeWindowStart.toISOString()
    }

    this.columns = [
      { id: 'icon', label: '', minWidth: 5 },
      { id: 'time', label: 'Time', minWidth: 170 },
      { id: 'severity', label: 'Severity', minWidth: 20 },
      { id: 'identifier', label: 'Identifier', minWidth: 100 },
      { id: 'message', label: 'Message', minWidth: 300 }
    ];

    this.rows = [];
    this.resultRows = [];

    this.styles = makeStyles({
      root: {
        width: '100%',
      },
      container: {
        maxHeight: 440,
      },
    });

    // Fetches new data from spotlight every 3s
    this.refreshInterval = 3000;
  }

  fetchExceptions() {
    var start = "";
    if ( this.state.lastStoreTime ) {
      start = "?start=" + this.state.lastStoreTime;
    }
    fetch(this.props.spotlighturl + "lastStoredEvents" + start)
      .then(res => res.json())
      .then(
        (result) => {
          // Inluding elements which are newly fired
          // If exception is aknowledged, then update the type of exception in the list
          var augmentedList = this.state.exceptionList;
          const receivedList = result.table.rows;
          for (const exception of receivedList) {
            if ( exception.type === "fire" ) {
              // If exception with this id is present, we remove it from the list
              const index = augmentedList.findIndex(element => element.exception === exception.exception);
              if ( index >= 0 ) {
                augmentedList.splice(index, 1);
              }
              // Adding exception to the first position of array
              augmentedList.unshift(exception);
            } else if ( exception.type === "rearm" ) {
              // Leaving an exception where it is but changing its type to "rearm" to indicate that it is aknowledged 
              const index = augmentedList.findIndex(element => element.exception === exception.exception);
              if ( index >= 0 ) {
                augmentedList[index].type = exception.type;
              }
            } else if ( exception.type === "revoke" ) {
              // Removing exception from array
              const index = augmentedList.findIndex(element => element.exception === exception.exception);
              if ( index >= 0 ) {
                augmentedList.splice(index, 1);
              }
            }
          }
          // Removing the oldest exceptions leaving only 1000
          if ( augmentedList.length > 1000 ) {
            augmentedList.splice(1000, augmentedList.length - 1000);
          }
          this.setState({
            error: null,
            isLoaded: true,
            exceptionList: augmentedList,
            lastStoreTime: result.lastStoreTime
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            error: error,
            isLoaded: true
          });
        }
      )
  }

  StickyHeadTable() {
    const classes = this.styles;
    const selectedID = this.state.selectedid;

    const handleChangePage = (event, newPage) => {
      this.setState({
        page: newPage,
      });
    };

    const handleChangeRowsPerPage = (event) => {
      this.setState({
        page: 0,
        rowsperpage: event.target.value
      });
    };

    const handleRowClick = (selectedID, type) => {
      this.setState({
       selectedid: selectedID
      });
      // Aknowledge exception after clicking on it (do not wait for response)
      if ( type === "fire" ) {
        fetch(this.props.spotlighturl + "rearm?exception=" + selectedID);
      }
    };

    return (
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader size="small" aria-label="sticky table">
            <TableHead>
              <TableRow>
                {this.columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {this.rows.slice(this.state.page * this.state.rowsperpage, this.state.page * this.state.rowsperpage + this.state.rowsperpage).map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code} onClick={() => {handleRowClick(row.exceptionid, row.type);}} selected={selectedID === row.exceptionid}>
                    {this.columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          <CellContent value={column.format && typeof value === 'number' ? column.format(value) : value} rowType={row.type} columnType={column.id}/>
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={this.rows.length}
          rowsPerPage={this.state.rowsperpage}
          page={this.state.page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    );
  }

  createData(time, identifier, message, uniqueid, type, severity) {
    //Formatting date
    var date = new Date(time * 1000);
    var readableDate = date.toUTCString();
    //Decoding message
    var decodedMessage = decodeURI(message);
    //Change severity text to lower case
    var lowerCaseSeverity = severity.toLowerCase();
    return ({
      time: readableDate,
      identifier: identifier,
      message: decodedMessage,
      exceptionid: uniqueid,
      type: type,
      severity: lowerCaseSeverity,
      icon: lowerCaseSeverity
    });
  }

  componentDidMount() {
    this.fetchExceptions();
    // Using a trick in order to access ExceptionList object pointer "this" inside fetchExceptions()
    // when called from a timer thread
    var self = this;
    this.interval = setInterval(function() { self.fetchExceptions(); }, this.refreshInterval);
  }

  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      this.rows = this.state.exceptionList.map((except) => {
        return this.createData(except.dateTime, except.identifier, except.message, except.exception, except.type, except.severity);
      });
      return (
        <div>
          <div>
            <LinearProgress />
          </div>
          <div className="exception-list-div">
            {this.StickyHeadTable()}
          </div>  
          <div className="exception-list-div">
            <ExceptionViewer exceptionid={this.state.selectedid} spotlighturl={this.props.spotlighturl}/>
          </div>
        </div>
      );
    }
  }
}

function HotspotLogo(props) {
  return (
    <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <section style={{ color: orange[500], fontSize: 20, fontWeight: 'bold', fontVariant: [ 'small-caps' ] }}>
          hotspot
        </section>
        <WhatshotIcon style={{ fontSize: 50, color: orange[500] }}/>
      </div>
    </div>
  );
}

class HotSpot extends React.Component {
  spotlighturl

  constructor(props) {
    super(props);
    const params = queryString.parse(this.props.location.search);
    const value = params.spotlighturl;
    if (typeof(value) !== "undefined" && value) {
      this.spotlighturl = value;
    } else {
      this.spotlighturl = "";
    }
  }

  render() {
    return (
      <div>
        <div>
          <HotspotLogo/>
        </div>
        <div>
          <ExceptionList spotlighturl={this.spotlighturl}/>
        </div>
      </div>
    );
  }
}

const HotSpotWithRouter = withRouter(HotSpot);

ReactDOM.render(
  <BrowserRouter>
    <Route path="/" component={ HotSpotWithRouter }/>
  </BrowserRouter>,
  document.getElementById('root')
);
