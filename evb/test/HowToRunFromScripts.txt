How to Run from Scripts
=======================

The event builder (EvB) is normally run and controller by
RCMS. However, it is also possible to run it without RCMS using
scripts provided within this package. There are 2 use cases: to run
stand-alone functionality tests of the EvB code and to measure the
performance of the event builder. This howto shall document how this
is done. All paths are given relative to the location of this howto
document.

The results of previous measurements are kept at
https://gitlab.cern.ch/cms-daq/measurements/evb


Environment settings
--------------------
The following environment variables need to be set before any scripts
can be run. A sample setting can be found in 'setenv.sh'.

- XDAQ_ROOT has to be set to the install location of the XDAQ release
  (typically /opt/xdaq).
- XDAQ_EVB can be set to a different path. If it is set, libevb.so is
  taken from this path. This is useful if you want to use a locally
  compiled version of the EvB.
- EVB_TESTER_HOME has to be set to the path of the EvB test directory,
  i.e. the directory where this file resides. This directory must be
  writeable to the user running the scripts.
- EVB_SYMBOL_MAP is the default name of the symbol map used for the
  stand-alone functionality tests (see below)
- PATH and PYTHONPATH have to contain the scripts directory,
  i.e. ${EVB_TESTER_HOME}/scripts


Stand-alone functionality tests
-------------------------------
The aim of these tests is to test the functionality of the EvB
applications. These test cases should capture basic functionality and
corner cases as far as they can be reproduced in a small setup.
EVB_SYMBOL_MAP should be set 'standaloneSymbolMap.txt'

The test cases are defined in the files found in the
test/cases directory. They are run on a single machine, e.g., the
virtual machine evb-unittests.cms. Some tests are sensitive to timing
and may fail on slower machines. It would be possible to run the tests
on several machines by using a different EVB_SYMBOL_MAP which
distributes the logical names to different machines.

The test cases inherit from the class TestCase, which does the heavy
lifting in providing the test infrastructure and utility
functions. Each test case has to provide 2 methods:
- runTest defines the steps executed during the test
- fillConfiguration defines the setup, i.e., the EVM, RUs and BUs
together with their configuration parameters.

The naming convention uses the number of EVM+RUs as the first number,
and the number of BUs as the second. I.e., 1x1 uses one EVM and on BU,
while 2x1 contains an additional RU. Note that the case class name has
to be identical to the file name to make the reflection work.

Some test use data generated inside the EVM/RU, i.e., the 'inputSource'
is set to 'Local'. A dummyFEROL is available in the EvB code base
which allows to emulate the TCP/IP stream otherwise received from the
FEROLs. The dummyFEROL sports several configuration parameters which
allows to emulate errors, e.g., skipped or duplicated events. A
rather sophisticated example can be found in 2x1_tolerateMismatch.py.

Before running any tests, you need to start the xdaqLauncher.py
process on the local machine which acts as simple daemon starting and
stopping the XDAQ processes:

    runTests.py --launcher start

The test cases can be run individually, by executing, e.g., for the
1x1.py case you would run

    runTests.py -v 1x1

You can run all tests defined in the cases directory by issuing

    runTests.py --all

Running without the verbose flag only return success or failure. In
the latter case, the last caught error is displayed. This is mainly
useful to find any tests which fail after code modifications.

The output of the tests is logged to test/logs, which is useful to
find out why a test failed. For each test there are 2 files:
- the .txt file contains the output of the test scripts including the
XML configuration given to the XDAQ processes
- the .log file contains the output as logged by the XDAQ process.

Note that some tests fail occasionally as the desired state is not
reached within the foreseen time. In this case, you best rerun the
test to verify if it succeeds on a second try. The stability of some
of the tests could be improved by using the SOAP replies from the XDAQ
process as used by RCMS. However, this has not been implemented in the
simplistic xdaqLauncher.py.

Once you are done with testing and want to release any resources, you
can stop the launchers with

    runTests.py --launcher stop



Performance measurements
------------------------
Performance measurements are using a local XML file containing the full
configuration and a corresponding symbol-map file containing the names
of the machines used in the configuration. These files need to be
created before any measurement can be run.

Creating configurations:

First you need to create or identify a FBset in the hwdb which
represents the setup to be tested. The scripts will switch the
generation mode to a free-running configuration independent of the
settings in the FBset. I.e. you can also use a FBset which has been
created for running with GTPe. However, the scripts do not allow for
masking any FEDs. Therefore, the FBset should contain only the FEDs
and FEDbuilders which shall be used for the measurements. If this is
not possible, you can edit the local XML file later (see below.)

You can find a FBset filler to create the EvB scaling setups at
https://gitlab.cern.ch/cms-rcms/cdaq/hw-fillers/-/tree/master/daq3val/rcms/utilities/hwcfg/fillers/daq3val/FillFBSet_EvBmeasurements.java

Once you have a suitable FBset, you can create the files needed for
running with scripts with e.g.:

    createConfig.py /daq2/eq_170224test/test/fb_PixelUpgradeFBs \
      /DAQ2_Official/StandAlone/StandAlone_40g_infini_dropAtBU \
      16 cdaq/20170227/pixelFB

    There are 4 mandatory parameters:
    - FBset (/daq2/eq_170224test/test/fb_PixelUpgradeFBs)
    - Software template containing the parameters
      (/DAQ2_Official/StandAlone/StandAlone_40g_infini_dropAtBU)
    - Number of BUs in the configuration (16)
    - Local output directory relative to the execution directory
      (cdaq/20170227/pixelFB)

    In addition, you can specify the following options
    --properties to use an alternative properties file to connect to
      the relevant DB (default is CONFIGURATOR.properties in your home
      directory)
    --useBlacklist to omit any hosts on the latest blacklist, or
    --hostList with a file as argument in which case only hosts in the
      file will be used. Note that this file has to contain at least
      one FU for each BU as otherwise the BU is blacklisted because it
      has no available FUs. This is also true regardless of data being
      dropped at the BU or not.
    --userSettings allows you to override exported template settings
      with parameters from the file given as argument
    --daqval changes the relevant parameters to generate a
      configuration suitable for daq2val instead of cDAQ

Note that at the time of writing (Sep 2020), the standard configurator
defined in createConfig.py (line 38) does not support the creation of
folded EvB configurations. You need to get a pre-release version from Maciej.

If createConfig.py runs successfully, you will find 3 files in the
output directory:
- pixelFB.xml which contains the full configuration. The file name
  corresponds to the last directory of the output directory. If
  needed, this file can be edited to make any necessary changes
  (e.g. remove some FEDs or FEDbuilders)
- symbolMap.txt has the mapping of symbolic names to actual
  hostnames. Note that the XML file uses a mixture of symbolic names
  and real hostnames. Therefore, if you need to change hosts after the
  fact, you have to change them in both files.
- setup.info is a text file giving a short summary of the
  configuration. This file is not used by the scripts.

You can find a convenience scripts createDaq3valConfigs.sh,
which creates the setups for the EvB scaling setups created
by FillFBSet_EvBmeasurements.java mentioned above. The latest
configurations are at
https://gitlab.cern.ch/cms-daq/measurements/evb/daq3val/configs/20200915.

In order to save disk space, you can manually gzip the XML file. The
scripts will uncompress the XML file internally in this case.


Running configurations:

Once you have a configuration, you can use it to run a scan over
different fragment sizes using e.g.:

    runScans.py cdaq/20170227/pixelFB --outputDir $outputDir \
      --logDir $logDir --ferolMode --short

    The only mandatory parameter is the path to the directory which
    contains the configuration. However, it is useful to specify
    additional parameters:
    --outputDir gives the path relative to the execution directory
      where the results of the scan are stored. You'll find there 2
      files: a txt file which contains summary output from the scan
      including the configuration snippets as sent to the
      applications, and a dat file which contains the raw
      measurements. This file can be used to plot the results
      (see below.)
    --logDir should point to a directory with enough quota
      (e.g. /globalscratch/$USER) as it will contain the detailled
      debug output of all XDAQ processes.
    --ferolMode should be used by default to generate the data on the
      FEROLs. Otherwise, events are generated on the FRL.
    --short is a shorthand for specifying the fragment sizes to be
      scanned. Alternatively, you can also specify --full, which uses
      more points, or you can specify a list of sizes with
      --sizes 2048 2560 4096 8192 12288 16384.
      Note that the fragment sizes are only applied to FEDs connecting
      to the RUs. The FED size for the first FED connected to the EVM
      is always fixed at 1024 Bytes, roughly corresponding to the
      TCDS fragment size


    By default, a new dat file is created when you re-run the same
    configuration with the same outputDir. The previous dat file is
    saved into a directory using the timestamp of the previous
    file. If you want to scan additional sizes, you can specify
    '--append', in which case the new measurements are appended to the
    existing file. This is useful if either the scan failed at a
    certain point, or if you scanned with the '--short' option and
    would like to scan the additional points contained in the '--full'
    option. There is a shorthand '--compl' which will scan the
    sizes contained in the long, but not in short option.


The standard configurations generated by the configurator use FEROLs
as input to the EVM/RU, i.e. the 'inputSource' is 'Socket', expecting
data delivered from pt::blit. There are several options in the scripts
which allow to override it:
   --dropAtRU drops the data after the superfragment has been built
   and its consistency has been checked. No data is sent to the BUs
   and all EVM/RUs run independently from each other.
   --dropAtStream drops the individual FED fragments after they have
   been chopped and checked. No superfragments are built and the
   corresponding monitoring counters will stay zero.
   --dropAtSocket drops the socket buffers right after they have been
   received from pt::blit. No checking is done and the monitoring data
   on the EVM/RU stays zero, except for the input bandwidth and
   socket-buffer rate.
   --generateAtRU drops the FEROL configurations, i.e., no FEROLs are
   used in this mode. Instead, dummy super-fragments are generated at
   the EVM/RUs. The super-fragments contain the proper FED structure
   with dummy payload containing 0xCA.
   --ferolsOnly is used to start the FEROLs only, without any EvB
   applications. In this case, a different receiver is needed to
  handle the TCP streams. This is useful to measure the network
  performance independently of any XDAQ application.


The output mode of the BUs can be overridden by the scripts, too:
   --dropAtBU drops the events once they are fully built and
   checked. The counters for the output (DiskWriter) stay zero.
   --writeDelete writes the events into files. However, once the
   file is closed, it is deleted instead of being moved to its final
   position. This allows to measure the full BU performance without
   involving the HLT or if no FUs are available.
   --hlt is the standard mode of operations when the data is processed
   by HLT. It can be used to override configurations which have been
   made with dropAtBU. If you run measurements with HLT, it is
   recommended to use the '--long' option to take into account the
   larger fluctuations in rate due to backpressure from HLT.


If you want to test the configuration or setup the system for
debugging purposes, you can use the following command:

    runScans.py cdaq/20170227/pixelFB --outputDir $logDir \
      --logDir $logDir --ferolMode --nbMeasurements 0 --sizes 4096

    In this mode, we don't care about the results. Therefore, the
    outputDir is set to the scratch directory $logDir. In addition,
    only one fragment size (4096 Bytes) is used.
    '--nbMeasurements 0' is a backdoor to inhibit the scan. In this
    case, the script waits for the return being pressed before
    proceeding. It is possible to specify multiple sizes in which case
    the script reconfigures the system for the next fragment size and
    waits again on the return key. If all sizes have been tried, the
    system is teared down. Note that it might be useful to run with
    '--verbose' option in which case you get all output also on the
    terminal.
    In case you want to drop the data at the callback of pt::blit,
    i.e. inhibit any super-fragment building, you can specify
    '--dropAtSocket'. In this case no rate or throughput is visible on
    the hyperdaq pages of the RU, and the measurements will be 0
    regardless if data is actually flowing or not. The option
    '--dropAtRU' activates the logic to drop the super-fragments after
    they have been built on the RU, i.e. the RUs run independently.


A typical example how to run several settings, e.g., for measuring the
scaling behavior of the RU for the configurations created by
createDaq3valConfigs.sh, could be:

    for nbFEDs in '4' '8' '12' '16' '20' '27' ; do
        runScans.py $confDir/${nbFEDs}x1x2 --outputDir $outputDir --logDir $logDir --ferolMode --writeDelete --short
    done


Sometimes it is useful to run from a modified XML file with a
different name, but using the same configuration directory, e.g. if
you want to scan different parameters. In this case, you need to start
and stop the launchers separately:

    runTests.py --symbolMap cdaq/20170227/pixelFB/symbolMap.txt \
      --launchers start --logDir $logDir
    runScans.py cdaq/20170227/pixelFB/myModifiedConfig.xml \
      --symbolMap cdaq/20170227/pixelFB/symbolMap.txt \
      --outputDir $outputDir --logDir $logDir --ferolMode --short
    runTests.py --symbolMap cdaq/20170227/pixelFB/symbolMap.txt \
      --launchers stop



The simple scans described above used an identical fragment size for
all FEDs. It is also possible to run a configuration using
parameterized FED sizes. The FED sizes are parametrized with a
polynom 3rd order a+bx+cx^2+dx^3. The parameters need to be specified
in a csv file which lists for each FED id the following parameters:
   fedId,a,b,c,d,rms
The rms for the log-normal distribution is specified relative to the
fragment size. The fragment size is set to 2048 Bytes for any FED id
which is not specified in this file. The EVM FED size will be set to
1024 Bytes unless the FED id feeding the EVM is found in the parameter
file. When running with parameterized FED sizes, it is more natural to
specify the relative fragment sizes x in above formula instead of
absolute values. The full command to a full scan looks like:

    runScans.py cdaq/20170131/ppFedBuilder --ferolMode \
      --calculateFedSizesFromFile fedSizes_run276870_Andre_rms.csv \
      -o $outDir --logDir $logDir \
      --relsize 1.0 1.25 1.5 2.0 2.5 0 0.25 0.5 0.75 3.0 3.5 4.0

The FED-size parameter files for pp and HI used for the initial
estimate for run 3 can be found at
https://gitlab.cern.ch/cms-daq/measurements/evb/daq3val/-/tree/master/configs/run3FB


There are a couple more options which might be helpful:

    --numa disables any NUMA settings.
      In line 185ff of test/Configuration.py you find some commented
      out code which shows how the NUMA settings could be overridden
      by the script. This is useful to make quick adjustments to the
      NUMA settings without the need to regenerate or modify the XML
      files.
    --RoCE allows to switch from IB to ETH network. Note that the
      network names and interfaces for daq3val are hardcoded in
      test/SymbolMap.py.
    --maxTriggerRate allows to limited the trigger rate in Hz. This is
      done by throttling the rate on the EVM by sleeping in the
      event-assignment thread.
    --waitForStateTransitions requires a key stroke before each state
      transition is initiated. This is helpful to investigate problems
      during state transitions.



Plotting results:

You need a pyROOT installation for plotting any of the results. The
easiest way to do it is to copy the dat files to your local machine
which has a ROOT installation. The script is called 'plotScans.py' and
takes one or more dat files as argument. A typical command looks like

    plotScans.py \
      ../20170215/1s1fx1x2.dat \
      1s1f40x1x2.dat \
      --legend \
      "1 Ferol (stream 0)" \
      "1 Ferol40 (stream 0)" \
      -o dataConcentrator_1streams.pdf \
      --subtag "1 stream #Rightarrow 1 RU #Rightarrow 2 BUs"


    plotScans.py \
      ../20160910/ppFedBuilder_splitTracker_doubleAlloc/ppFedBuilder_splitTracker.dat \
      ../20170112/ppFedBuilder_splitTracker/ppFedBuilder_splitTracker.dat \
      ppFedBuilder_splitTracker/ppFedBuilder_splitTracker.dat \
      --legend \
      'CHEP' \
      'XDAQ13' \
      'XDAQ14' \
      -o ppFedBuilder_splitTracker.pdf \
      --tag '78+1 RUs #Rightarrow 73 BUs' --subtag "Real FED distribution and sizes" \
      --totalThroughput --minx 620 --maxx 2580 --maxy 320 --ratemax 280 --nologx


FEDbuilder Performance
----------------------
The FEDbuilders in the production system do not use the simplistic
approach of a fixed number of FEROLs per RU. The fragment sizes for
different FEDs/subsystems are vastly different. Therefore, the
FEDbuilders contain different number of FEDs. In addition, FEDbuilders
only combine FEDs belonging to the same subsystem. The design of the
FEDbuilders needs to take into account the throughput capability of
the RU, too. This depends on the number of streams and the FED
sizes. Typically, the RU can handle a higher rate of small FED sizes
from more streams and less streams from FEDs with large fragments.

There are two methods to assess the throughput of the FEDbuilders:
estimate the throughput based on the FBset created by the RCMS
configurator in the HWCfg DB, and to measure the actual performance
using the test bed, e.g. daq3val.


Throughput based on the FBset:

(1) Use the DAQ configurator to display the FBset structure.

(2) Copy the text into a text file. An example of the run 2 FBset
definition can be found at
https://gitlab.cern.ch/cms-daq/measurements/evb/daq3val/-/blob/master/configs/run3FB/run2FB.txt

(3) Use the FED size parameter file to calculate the expected
throughput:

    checkFBthroughput.py --plain run2FB.txt fedSizes-run3-pp.csv 1

checkFBthroughput.py contains different models for the maximum
throughput for different number of FEROL streams. These parameters can
be obtained by using the '--fit' option in plotScans.py. Note that
there are two fit models used: a linear and quadratic plateau model as
defined in FitModels.py. You need to decide case by case which of the
models yield a more accurate description. In any case, these can
only give an approximate estimate if the FED builder would be able to
cope with the FED sizes.

checkFBthroughput.py has additional parameters:
   --triggerRate sets the expected L1 trigger rate. The default is
   100 kHz, which needs to be lowered in case of heavy-ion FED sizes.
   --printAll shows the throughput of all FEDbuilders, not only of
   those which have a too high throughput
   --plain is used to read the flat text file. Without this option,
   the input must be a full DAQ configuration in XML format. (This
   option has not been tested with configurations with a folded RUBU.)



Actual performance measurement in daq3val:

There are not enough FEROLs available in the testbed to measure all
FED builders simultaneously. Therefore, the idea is to create separate
configurations for each FEDbuilder, which contains only the EVM on a
separate host and the RUBU handling the given FEDbuilder. The aim is
to measure the achievable throughput and L1 trigger rate for the
expected fragment sizes. The performance of the RUBU node is
influenced by the BU: a single BU in the system has to digest a much
higher event rate than in standard running, where the rate is
distributed over several BUs. However, having multiple BUs in the
system causes the BU on the RUBU node to request less events than its
less loaded neighbors. Therefore, it is advisable to measure the
performance with one and two BUs. The truth lies most likely somewhere
between these two settings. The results of the test run in July 2020
can be found at https://twiki.cern.ch/twiki/bin/view/CMS/DAQ3fedbuilders


(1) Manually create two XML templates of the full system with the
maximum number of FEDs foreseen in a FED builder, once with one BU on
the RUBU node and once with an additional BU on the EVM node. Note
that the FED ids are replaced by FEDID01..FEDID55.

An example (not containing the latest settings from the SWT) can be found at
https://gitlab.cern.ch/cms-daq/measurements/evb/daq3val/-/tree/master/configs/run3FB

(2) Edit makeFBconfigs.sh in above URL to define the fbDef file,
which contains the plain text FB set definitions, and the confDir,
which is used to store the output.

(3) Run makeFBconfigs.sh, which will create the XML configurations for
all FEDbuilders.

(4) Perform the measurements by editing and executing doFBscans.sh in
the daq3val directory. Note that the it takes several days to run the
measurements for all FEDbuilders. Therefore, it might be useful to
grep for certain FEDbuilder names (c.f. the code in doFBscans.sh)

(5) The plots shown on the twiki page are generated using
https://gitlab.cern.ch/cms-daq/measurements/evb/daq3val/-/blob/master/results/FEDbuilders/plots.sh



Setting FED sizes with RCMS
---------------------------
The DAQ FM can configure individual FED sizes, too. This is useful to
assess the full DAQ performance in emulator mode. However, it is not
possible to run scans with different FED sizes in an automated
way.

To run with individual FED sizes, you need to set the exported
global variable GET_FED_FRAGMENT_SIZES_FROM_FILE to 'true' in the
.cmsOneStep configurator. In this case, the DAQ FM sets the FED sizes
according to the file /nfshome0/daqoncall/fedFragmentSizes.csv.
In September 2020, this is a symlink to
fedFragmentSizes/fedFragmentSizes_HI2018.csv, which has the best
estimate of the HI fragment sizes from 2018. You need to re-configure
DAQ whenever you changed the symlink to another file to pick up the
changes.

The format of the file read by the DAQ FM is different from the
parameter file used by the EvB scripts which contains the parameters
of a 3rd-degree polynominal (c.f. above). The file read by the DAQ FM
has the format
  fedId,meanSize,rmsSize
where the mean size is in Bytes and the rms size gives the width of
the log-normal distribution in Bytes. There might be further columns,
which are ignored by the DAQ FM.

There is a script test/scripts/createFedSizeFile.py in the EvB
repository which allows to create the FED-size file for the DAQ FM
from the parameter file used for the EvB measurements:

    createFedSizeFile.py paramFile relEventSize outputFile

    paramFile is the file which contains the fit parameters.
      It corresponds to the parameter 'calculateFedSizesFromFile' of
      runScans.py.
    relEventSize specifies the relative FED sizes to use
      It corresponds to one of the 'relsize' of runScans.py.
    outputFile specifies the file name which is can be read by the DAQ FM.



EvB Benchmark
-------------
This benchmark uses the standard EvB code and protocols to measure
network throughputs. The benchmark script creates configurations on
the fly for arbitrary numbers of RUs and BUs, and also for folded
RUBUs. Note that the EVM is kept separate in all cases. The script
also supports mixed configurations, i.e. N RUs with M BUs and K RUBUs.
This benchmark script also allows to use the GEVB application (--useGEVB)
instead of the EvB application.

Each RU(BU) generates the data for 8 FEROL streams locally, and the
(RU)BU drops the completed events, i.e. they are not written to
disk. The configuration also takes care of the NUMA settings. It uses
the default settings for the unfolded EvB, and adopts the values found
by Andre's genetic algorithm for the folded one. It can also deal with
the dvrubu nodes where the IB and ETH cards are on the other socket
compared to the production setup.

To test a configuration for a single fragment, you can use

   runBenchmarks.py -o $outputDir --logDir $logDir \
       --nbMeasurements 0 --sizes 2048 \
       --nRUs 0 --nBUs 0 --nRUBUs 2 --symbolMap cases/dvrubuSymbolMap.txt

    --outputDir gives the path relative to the execution directory
      where the results of the scan are stored. You'll find there 2
      files: a txt file which contains summary output from the scan
      including the configuration snippets as sent to the
      applications, and a dat file which contains the raw
      measurements. This file can be used to plot the results
      (see above.)
    --logDir should point to a directory with enough quota
      (e.g. /globalscratch/$USER) as it will contain the detailled
      debug output of all XDAQ processes.
    --nbMeasurements 0 is a backdoor to inhibit the scan. In this
      case, the script waits for the return being pressed before
      proceeding. It is possible to specify multiple sizes in which case
      the script reconfigures the system for the next fragment size and
      waits again on the return key. If all sizes have been tried, the
      system is teared down. Note that it might be useful to run with
      '--verbose' option in which case you get all output also on the
      terminal.
    --nRUs defines the number of RUs to be used (default is 1)
    --nBUs defines the number of BUs to be used (default is 1)
    --nRUBUs defines the number of RUBUs (default is 0)
    --symbolMap gives the relative path to the symbol map defining the
      mapping between logical and physical names. It overrides the one
      set by the environment variable EVB_SYMBOL_MAP. The symbolMap
      file must contain at least as many nodes as RUs, BUs, and RUBUs
      are requested on the command line. RUBUs are treated as RUs.

Other optional parameters:
    --oneFEDxRU uses only one stream in the RU (no superfragment builing). 
    --useGEVB uses the GEVB application instead of EvB application.

To run a full scan, the same parameters can be used as for the
standard performance scan (see above).
