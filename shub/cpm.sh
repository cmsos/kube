#! /bin/bash

display_usage() {
	echo -e "Copy measurments (.dat files) from runner to destination directory named path/measurements"
	echo -e "\nUsage: $0 runnerid path \n"
	}
# if less than two arguments supplied, display usage
	if [  $# -le 1 ]
	then
		display_usage
		exit 1
	fi

# check whether user had supplied -h or --help . If yes display usage
	if [[ ( $# == "--help") ||  $# == "-h" ]]
	then
		display_usage
		exit 0
	fi




kubectl cp shub/$1:tmp $2/measurements
