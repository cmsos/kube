#!/usr/bin/python3

from string import Template
import getopt, sys

#Args used to parameterize 
numreplicas=1
interface='ens3f0'
gidtype='v2'
port=0
endpointhost='endpoint'
endpointport=0
endpointdomain='roce'

full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hr:i:t:p:e:m:f:"
long_options = ["help", "replicas=", "interface=", "gidtype=", "port=", "endpointhost=", "endpointport=", "endpointdomain="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: autogen.py [-r #,--replicas=#] [-i name,--interface=name] [-t type,--gidtype=type] [-p # ,--port=#]  [-e name,--endpointhost=name] [-m name,--endpointdomain=name] [-f # ,--endpointport=#]")
        sys.exit(0)
    elif current_argument in ("-r", "--replicas"):
        numreplicas = int(current_value)
    elif current_argument in ("-i", "--interface"):
        interface = current_value
    elif current_argument in ("-t", "--gidtype"):
        gidtype = current_value
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-e", "--endpointhost"):
        server = current_value
    elif current_argument in ("-m", "--endpointdomain"):
        domain = current_value
    elif current_argument in ("-f", "--endpointport"):
        endpointport = int(current_value)

#max block size * number of resources * number of replicas
rusenderpoolsize = 0x40000 * 120 * numreplicas
#max block size * (max block size * 2 + receive queue pair + margin) * number of replicas
bureceiverpoolsize = 0x40000 * (120 * 2 + 384 + 2) * numreplicas

#Set dictionary for template expansion
dict = {'endpointhost':endpointhost, 'endpointport':endpointport, 'endpointdomain':endpointdomain,
        'interface':interface, 'gidtype':gidtype, 'port':port, 'replica':'0',
        'rusenderpoolsize':rusenderpoolsize, 'bureceiverpoolsize':bureceiverpoolsize}
#Templates XML 

i2otarget=Template("""  <i2o:target class="$classname" instance="$replica" tid="$tid"/>""")

rut = Template("""
    <xc:Context url="http://ru-$replica.ru:$port">
        <xc:Endpoint protocol="ibv" service="i2o" hostname="ru-$replica.ru" port="$endpointport" network="infini"/>

        <xc:Application class="pt::ibv::Application" id="11" instance="$replica" network="local">
            <properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
                <networkInterface xsi:type="xsd:string">$interface</networkInterface>
                <GIDType xsi:type="xsd:string">$gidtype</GIDType>
                <portNumber xsi:type="xsd:unsignedInt">1</portNumber>
                <receiverPoolSize xsi:type="xsd:unsignedLong">0x2000000</receiverPoolSize>
                <senderPoolSize xsi:type="xsd:unsignedLong">$rusenderpoolsize</senderPoolSize>
                <completionQueueSize xsi:type="xsd:unsignedInt">90000</completionQueueSize>
                <sendQueuePairSize xsi:type="xsd:unsignedInt">240</sendQueuePairSize>
                <recvQueuePairSize xsi:type="xsd:unsignedInt">64</recvQueuePairSize>
                <maxMessageSize xsi:type="xsd:unsignedInt">0x3fff0</maxMessageSize>
                <deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
            </properties>
        </xc:Application>
        
        <xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
        <xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

        <xc:Application class="xmem::probe::Application" id="21" instance="0" network="local">
        </xc:Application>
        <xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>

        <xc:Application class="gevb2g::InputEmulator" id="42" instance="$replica" network="infini">
            <properties xmlns="urn:xdaq-application:gevb2g::InputEmulator" xsi:type="soapenc:Struct">
                <StdDev xsi:type="xsd:unsignedLong">256</StdDev>
                <Mean xsi:type="xsd:unsignedLong">12</Mean>
                <MinFragmentSize xsi:type="xsd:unsignedLong">256</MinFragmentSize>
                <MaxFragmentSize xsi:type="xsd:unsignedLong">32768</MaxFragmentSize>
                <destinationClassName xsi:type="xsd:string">gevb2g::RU</destinationClassName>
                <destinationClassInstance xsi:type="xsd:unsignedLong">0</destinationClassInstance>
                <maxDataFrameSize xsi:type="xsd:unsignedLong">262144</maxDataFrameSize>
                <frameSendCounter xsi:type="xsd:unsignedLong">0</frameSendCounter>
                <fixedSize xsi:type="xsd:boolean">true</fixedSize>
                <rate xsi:type="xsd:unsignedLong">10000000</rate>
                <createPool xsi:type="xsd:boolean">false</createPool>
                <poolName xsi:type="xsd:string">sibv</poolName>
            </properties>
        </xc:Application>

        <xc:Application class="gevb2g::RU" id="43" instance="$replica" network="infini">
            <properties xmlns="urn:xdaq-application:gevb2g::RU" xsi:type="soapenc:Struct">
                <maxRequestsQueue xsi:type="xsd:unsignedLong">16384</maxRequestsQueue>
                <inputDataFifoSize xsi:type="xsd:unsignedLong">150000</inputDataFifoSize>
                <maxDataFrameSize xsi:type="xsd:unsignedLong">262144</maxDataFrameSize>
                <frameSendCounter xsi:type="xsd:unsignedLong">0</frameSendCounter>
                <preAllocateDAPL xsi:type="xsd:unsignedLong">0</preAllocateDAPL>
                <doPacking xsi:type="xsd:boolean">false</doPacking>
                <packingSize xsi:type="xsd:unsignedInt">65536</packingSize>
                <createPool xsi:type="xsd:boolean">false</createPool>
                <poolName xsi:type="xsd:string">sibv</poolName>
            </properties>
        </xc:Application>

        <xc:Module>/opt/xdaq/lib/libgevb2g.so</xc:Module>

    </xc:Context>""")
    
but = Template("""
    <xc:Context url="http://bu-$replica.bu:$port">
        <xc:Endpoint protocol="ibv" service="i2o" hostname="bu-$replica.bu" port="$endpointport" network="infini"/>

        <xc:Application class="pt::ibv::Application" id="11" instance="$replica" network="local">
            <properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
                <networkInterface xsi:type="xsd:string">$interface</networkInterface>
                <GIDType xsi:type="xsd:string">$gidtype</GIDType>
                <portNumber xsi:type="xsd:unsignedInt">1</portNumber>
                   <receiverPoolSize xsi:type="xsd:unsignedLong">$bureceiverpoolsize</receiverPoolSize>
                   <senderPoolSize xsi:type="xsd:unsignedLong">0x4000000</senderPoolSize>
                   <completionQueueSize xsi:type="xsd:unsignedInt">32768</completionQueueSize>
                   <sendQueuePairSize xsi:type="xsd:unsignedInt">64</sendQueuePairSize>
                   <recvQueuePairSize xsi:type="xsd:unsignedInt">384</recvQueuePairSize>
                   <maxMessageSize xsi:type="xsd:unsignedInt">0x3fff0</maxMessageSize>
                   <deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
            </properties>
        </xc:Application>
        
        <xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
        <xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

        <xc:Application class="xmem::probe::Application" id="21" instance="0" network="local">
        </xc:Application>
        
        <xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>

        <xc:Application class="gevb2g::BU" id="45" instance="$replica" network="infini">
            <properties xmlns="urn:xdaq-application:gevb2g::BU" xsi:type="soapenc:Struct">
                <maxResourcesPerFrame xsi:type="xsd:unsignedLong">40</maxResourcesPerFrame>
                <maxResources xsi:type="xsd:unsignedLong">120</maxResources>
                <maxDataFrameSize xsi:type="xsd:unsignedLong">262000</maxDataFrameSize>
                <filterDisable xsi:type="xsd:boolean">true</filterDisable>
                <numberOfSamples xsi:type="xsd:unsignedLong">20</numberOfSamples>
                <sampleTime xsi:type="xsd:string">PT5S</sampleTime>
                <createPool xsi:type="xsd:boolean">false</createPool>
                <poolName xsi:type="xsd:string">sibv</poolName>
            </properties>
        </xc:Application>
        <xc:Module>/opt/xdaq/lib/libgevb2g.so</xc:Module>

    </xc:Context>""")


evmt = Template("""
    <xc:Context url="http://evm-$replica.evm:$port">
        <xc:Endpoint protocol="ibv" service="i2o" hostname="evm-$replica.evm" port="$endpointport" network="infini"/>
        <xc:Application class="pt::ibv::Application" id="11" instance="0" network="local">
            <properties xmlns="urn:xdaq-application:pt::ibv::Application" xsi:type="soapenc:Struct">
                <networkInterface xsi:type="xsd:string">$interface</networkInterface>
                <GIDType xsi:type="xsd:string">$gidtype</GIDType>
                <portNumber xsi:type="xsd:unsignedInt">1</portNumber>
                <receiverPoolSize xsi:type="xsd:unsignedLong">0x80000000</receiverPoolSize>
                <senderPoolSize xsi:type="xsd:unsignedLong">0xA95C300</senderPoolSize>
                <completionQueueSize xsi:type="xsd:unsignedInt">65536</completionQueueSize>
                <sendQueuePairSize xsi:type="xsd:unsignedInt">1024</sendQueuePairSize>
                <recvQueuePairSize xsi:type="xsd:unsignedInt">512</recvQueuePairSize>
                <maxMessageSize xsi:type="xsd:unsignedInt">65300</maxMessageSize>
                <deviceMTU xsi:type="xsd:unsignedInt">4096</deviceMTU>
            </properties>
        </xc:Application>
        <xc:Module>/opt/xdaq/lib/libibvla.so</xc:Module>
        <xc:Module>/opt/xdaq/lib/libptibv.so</xc:Module>

        <xc:Application class="xmem::probe::Application" id="22" instance="0" network="local"></xc:Application>
        <xc:Module>/opt/xdaq/lib/libxmemprobe.so</xc:Module>

        <xc:Application class="gevb2g::EVM" id="43" instance="0" network="infini">
            <properties xmlns="urn:xdaq-application:gevb2g::EVM" xsi:type="soapenc:Struct">
                <triggerDisable xsi:type="xsd:boolean">true</triggerDisable>
                <triggerQueueSize xsi:type="xsd:unsignedLong">65536</triggerQueueSize>
                <resourceQueueSize xsi:type="xsd:unsignedLong">65536</resourceQueueSize>
                <maxGatherRequests xsi:type="xsd:unsignedLong">1072</maxGatherRequests>
                <triggerGather xsi:type="xsd:unsignedLong">16384</triggerGather>
                <triggerClassName xsi:type="xsd:string">unknwon</triggerClassName>
                <triggerClassInstance xsi:type="xsd:unsignedLong">0</triggerClassInstance>
                <createPool xsi:type="xsd:boolean">false</createPool>
                <poolName xsi:type="xsd:string">sibv</poolName>
            </properties>
        </xc:Application>
        <xc:Module>/opt/xdaq/lib/libgevb2g.so</xc:Module>
</xc:Context>""")

#Generate configuration XML 


print("""<xc:Partition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30">""")
print("""<i2o:protocol xmlns:i2o="http://xdaq.web.cern.ch/xdaq/xsd/2004/I2OConfiguration-30">""")
t = 24
i = i2otarget.substitute(replica='0', tid=t, classname='gevb2g::EVM')
print (i)
t += 1
for x in range(numreplicas):
    i = i2otarget.substitute(replica=x, tid=t, classname='gevb2g::InputEmulator')
    print (i)
    t += 1
    i = i2otarget.substitute(replica=x, tid=t, classname='gevb2g::RU')
    print (i)
    t += 1
    i = i2otarget.substitute(replica=x, tid=t, classname='gevb2g::BU')
    print (i)
    t += 1
print("""</i2o:protocol>""")
a = evmt.substitute(dict)
print (a)
for x in range(numreplicas):
    dict['replica'] = x
    r = rut.substitute(dict)
    print (r)
    b = but.substitute(dict)
    print (b)
print ("""</xc:Partition>""")
