if(format eq 'jpeg'){
  set terminal jpeg
  set output "startup_plot_detailed.jpg"
}else{
  if(format eq 'eps'){
    set terminal postscript eps enhanced color size 10cm,6.5cm "Times-Roman" 18
    set output "startup_plot_detailed.eps"
  }
}

set linetype  1 lc rgb "dark-violet" lw 1
set linetype  2 lc rgb "#009e73" lw 1
set linetype  3 lc rgb "#56b4e9" lw 1
set linetype  4 lc rgb "#e69f00" lw 1
set linetype  5 lc rgb "#f0e442" lw 1
set linetype  6 lc rgb "#0072b2" lw 1
set linetype  7 lc rgb "#e51e10" lw 1
set linetype  8 lc rgb "black"   lw 1
set linetype  9 lc rgb "gray50"  lw 1
set linetype cycle  9

set yrange [0:30]
set xrange [0:106]

set xlabel 'Number of pods'
set ylabel 'Time, s'

plot 'pod_scheduled_data.txt' using ($1*2):2:3:4 with yerrorbars lt 1 title 'pod scheduled', \
     'pod_scheduled_data.txt' using ($1*2):2 with lines lt 1 notitle, \
     'containers_ready_data.txt' using ($1*2):2:3:4 with yerrorbars lt 2 title 'containers ready', \
     'containers_ready_data.txt' using ($1*2):2 with lines lt 2 notitle, \
     'app_alive_data.txt' using ($1*2):2:3:4 with yerrorbars lt 3 title 'app alive', \
     'app_alive_data.txt' using ($1*2):2 with lines lt 3 notitle, \
     'pod_connectable_data.txt' using ($1*2):2:3:4 with yerrorbars lt 4 title 'pod connectable', \
     'pod_connectable_data.txt' using ($1*2):2 with lines lt 4 notitle, \
     'app_ready_data.txt' using 1:2:3:4 with yerrorbars lt 5 title 'app ready', \
     'app_ready_data.txt' using 1:2 with lines lt 5 notitle, \
     'app_connected_data.txt' using 1:2:3:4 with yerrorbars lt 6 title 'app connected', \
     'app_connected_data.txt' using 1:2 with lines lt 6 notitle
