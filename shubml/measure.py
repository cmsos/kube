import rest

def retrieve(port,replicas):
    sample = {}
    m = {}
    fragmentRate = 0
    for r in range(replicas):
        m['BU'+ str(r)] = int(rest.get("http://node-" + str(r) + ".node:" + str(port) + "/api/service/noderc/property/eventRate"))
        fragmentRate += int(m['BU'+ str(r)])
    for r in range(replicas):
        m['RU'+ str(r)] = fragmentRate
    m['EVM0'] = fragmentRate

    sample['rates'] = m;

    s  = {}
    s['EVM0'] = 0
    eventSize = 0
    for r in range(replicas):
        s['RU'+ str(r)] = int(rest.get("http://node-" + str(r) + ".node:" + str(port) + "/api/service/noderc/property/superFragmentSize"))
        eventSize += int(s['RU'+ str(r)])
    for r in range(replicas):
        s['BU'+ str(r)] = eventSize
    sample['sizes'] = s;

    print ("retrieved sample " + str(sample))
    return sample
