#include <iostream>
#include <stdexcept>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <sstream>
#include <unistd.h>

int listenfd_;

void listen(const std::string & hostname, const std::string & port, int num)
{
	struct addrinfo *res, *t;
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	int ret;
	
	ret = getaddrinfo(hostname.c_str(), port.c_str(), &hints, &res);
	
	if (ret < 0)
	{
		std::cout << "Failed to get address info for host:port '" << hostname << ":" << port << ", " << gai_strerror(ret) << std::endl;
	}
	
	for (t = res; t; t = t->ai_next)
	{
		errno = 0;
		listenfd_ = socket(t->ai_family, t->ai_socktype, t->ai_protocol);
		if (listenfd_ < 0)
		{
			std::cout << "Failed to create socket on host:port '" << hostname << ":" << port << ", " << strerror(errno) << std::endl;
		}
		
		errno = 0;
		int n = 1;
		int ret = setsockopt(listenfd_, SOL_SOCKET, SO_REUSEADDR, &n, sizeof n);
		if (ret < 0)
		{
			::close(listenfd_);
			std::cout << "Failed to set socket options on host:port '" << hostname << ":" << port << ", " << strerror(errno) << std::endl;
		}
		
		errno = 0;
		ret = bind(listenfd_, t->ai_addr, t->ai_addrlen);
		if (ret != 0)
		{
			::close(listenfd_);
			std::cout << "Failed to bind socket on host:port '" << hostname << ":" << port << ", " << strerror(errno) << std::endl;
		}
		break;
	}
	
	freeaddrinfo(res);
	
	ret = ::listen(listenfd_, num);
	if (ret != 0)
	{
		::close(listenfd_);
		std::cout << "Failed to listen on host:port '" << hostname << ":" << port << ", " << strerror(errno) << std::endl;
	}	
}

void accept()
{
	errno = 0;
	
	struct sockaddr_in readAddr;
	socklen_t readAddrLen = sizeof(readAddr);
	
	int connfd = ::accept(listenfd_, (struct sockaddr *) &readAddr, &readAddrLen);
	
	if (connfd < 0)
	{
		std::cout << "Failed to accept on " << "TBD name" << ", " << strerror(errno) << std::endl;
	}
	else
	{
		std::cout << "Connection accepted" << std::endl;
	}
	
	std::string ip;
	ip = inet_ntoa(readAddr.sin_addr);
	struct hostent * h = gethostbyaddr((char*) &readAddr.sin_addr.s_addr, sizeof(readAddr.sin_addr.s_addr), AF_INET);
	std::string remoteHostName;
	if (h != 0)
	{
		remoteHostName = h->h_name;
	}
	else
	{
		remoteHostName = "";
	}
	
	std::cout << "Remote hostname: " << remoteHostName << std::endl;
	std::cout << "Remote IP address: " << ip << std::endl;
}

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		std::cout << "Usage: ./server <hostname> <port>" << std::endl;
		return -1;
	}

	std::cout << "Server is starting..." << std::endl;

	listen(argv[1], argv[2], 128);
	while (1)
	{	
		accept();
	}

	return 0;
}
